INSERT INTO connections(id, username, name, description, type, gora_properties, hadoop_configuration)
VALUES
	(1, 'admin', 'HBase @ localhost', 'Locally deployed HBase installation', 'HBASE', 'hbase.client.autoflush.default=false
gora.datastore.scanner.caching=10
gora.datastore.default=org.apache.gora.hbase.store.HBaseStore', '<?xml version="1.0"?>
<configuration>
</configuration>
'),
	(2, 'admin', 'HBase2 @ localhost', 'Locally deployed HBase installation', 'HBASE', 'hbase.client.autoflush.default=false
gora.datastore.scanner.caching=10
gora.datastore.default=org.apache.gora.hbase.store.HBaseStore', '<?xml version="1.0"?>
<configuration>
</configuration>
')
;