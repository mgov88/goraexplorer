/*******************************************************************************
 * Copyright 2018 Alfonso Nishikawa
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.nishilua.goraexplorer.request;

import java.io.IOException;
import org.springframework.core.convert.converter.Converter;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Converter needed to convert from a JSON RequestParam (HTTP encoded) to a Filters object.
 */
public class JsonToFiltersConverter implements Converter<String, Filters> {

    static ObjectMapper objectMapper = new ObjectMapper() ; 
    
    @Override
    public Filters convert(String source) {
        try {
            Filters filters = objectMapper.readValue(source, Filters.class) ;
            return filters ;
        } catch (IOException e) {
            throw new RuntimeException(e) ;
        }
    }

}
