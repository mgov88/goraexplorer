/*******************************************************************************
 * Copyright 2018 Alfonso Nishikawa
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.nishilua.goraexplorer.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableList;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    
    @Bean
    public Docket swaggerSpringMvcPlugin() {
        
        String version = getClass().getPackage().getImplementationVersion() ;
      
        ApiInfo apiInfo = new ApiInfoBuilder()
                .title("GoraExplorer REST API")
                .description("NoSQL databases manager REST API")
                .contact("Alfonso Nishikawa - http://www.nishilua.com/alfonso.nishikawa - alfonso.nishikawa@gmail.com")
                .license("GPLv3")
                .licenseUrl("https://www.gnu.org/licenses/gpl-3.0.en.html")
                .version(version == null ? "0.0.0.0" : version) // pom.xml version copied to war in maven-war-plugin, archive>manifest>addDefaultImplementationEntries
                .build();
        
        @SuppressWarnings("unchecked")
        Predicate<String> paths = Predicates.or(PathSelectors.regex("/.*")) ;
        
        ModelRef emptyModelRef = new ModelRef("") ;
        
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("goraexplorer-api")
                .select().paths(paths).build()
                .useDefaultResponseMessages(true)
                .globalResponseMessage(
                        RequestMethod.GET,
                        ImmutableList.<ResponseMessage>of(
                            new ResponseMessage(200, "OK", emptyModelRef),
                            new ResponseMessage(401, "Unauthorized if not logged in", emptyModelRef),
                            new ResponseMessage(403, "Unauthorized if not enough permissions", emptyModelRef)))
                .globalResponseMessage(
                        RequestMethod.POST,
                        ImmutableList.<ResponseMessage>of(
                            new ResponseMessage(200, "OK", emptyModelRef),
                            new ResponseMessage(401, "Unauthorized if not logged in", emptyModelRef),
                            new ResponseMessage(403, "Unauthorized if not enough permissions", emptyModelRef)))
                .globalResponseMessage(
                        RequestMethod.PUT,
                        ImmutableList.<ResponseMessage>of(
                            new ResponseMessage(200, "OK", emptyModelRef),
                            new ResponseMessage(401, "Unauthorized if not logged in", emptyModelRef),
                            new ResponseMessage(403, "Unauthorized if not enough permissions", emptyModelRef)))
                .globalResponseMessage(
                        RequestMethod.DELETE,
                        ImmutableList.<ResponseMessage>of(
                            new ResponseMessage(200, "OK", emptyModelRef),
                            new ResponseMessage(401, "Unauthorized if not logged in", emptyModelRef),
                            new ResponseMessage(403, "Unauthorized if not enough permissions", emptyModelRef)))
                .apiInfo(apiInfo) ;
    }
    
}
