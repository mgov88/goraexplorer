/*******************************************************************************
 * Copyright 2018 Alfonso Nishikawa
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.nishilua.goraexplorer.tables;

public enum ConnectionType {
    HBASE("org.apache.gora.hbase.store.HBaseStore"),
    CASSANDRA("org.apache.gora.cassandra.store.CassandraStore"),
    ACCUMULO("org.apache.gora.accumulo.store.AccumuloStore"),
    AVRO("org.apache.gora.avro.store.AvroStore"),
    DATAFILEAVRO("org.apache.gora.avro.store.DataFileAvroStore"),
    MEM("org.apache.gora.memory.store.MemStore"),
    MONGODB("org.apache.gora.mongodb.store.MongoStore"),
    SOLR("org.apache.gora.solr.store.SolrStore"),
    KUDU("org.apache.gora.kudu.store.KuduStore");
    IGNITE("org.apache.gora.ignite.store.IgniteStore");
    
    private String dataStoreClassName;
    
    ConnectionType(String dataStoreClassName) {
        this.dataStoreClassName = dataStoreClassName ;
    }

    public String getDataStoreClassName() {
        return dataStoreClassName;
    }

    public void setDataStoreClassName(String dataStoreClassName) {
        this.dataStoreClassName = dataStoreClassName;
    }

}
