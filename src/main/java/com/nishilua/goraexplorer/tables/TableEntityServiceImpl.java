/*******************************************************************************
 * Copyright 2018 Alfonso Nishikawa
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.nishilua.goraexplorer.tables;

import java.io.IOException;
import java.security.Principal;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.gora.persistency.Persistent;
import org.apache.gora.persistency.impl.PersistentBase;
import org.apache.gora.query.Query;
import org.apache.gora.query.Result;
import org.apache.gora.store.DataStore;
import org.apache.gora.util.GoraException;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.nishilua.goraexplorer.exceptions.CannotAccessResourceException;
import com.nishilua.goraexplorer.exceptions.InternalServerErrorException;
import com.nishilua.goraexplorer.tables.json.DeepDiffHelper;
import com.nishilua.goraexplorer.tables.request.TableRequest;

@Service("tableEntityService")
public class TableEntityServiceImpl implements TableEntityService {

    @PersistenceContext
    private EntityManager em ;
    
    @Autowired
    private TableEntityRepository tableEntityRepository ;

    @Autowired
    private ConnectionRepository connectionRepository ;

    @Autowired
    private ConnectionService connectionService;
    
    @Autowired
    private DataStoreManager dataStoreManager ;
    
    /**
     * Object mapper for updating operation from JsonNodes to Entity
     */
    private static ObjectMapper JSONObjectMapper = new ObjectMapper() ;
    
    @Override
    @Transactional(isolation=Isolation.READ_UNCOMMITTED, readOnly=true)
    public Collection<TableEntity> getTables(Long connectionId, Principal principal) {
        if ( !this.connectionService.checkConnectionBelongsToUser(connectionId, principal) ) {
            throw new CannotAccessResourceException(String.format("The user %s cannot access connection %d", principal.getName(), connectionId)) ;
        }

        return this.tableEntityRepository.findTablesByConnectionId(connectionId) ;
    }

    @Override
    @Transactional(isolation=Isolation.READ_UNCOMMITTED, readOnly=true)
    public TableEntity getTable(Long tableId, Principal principal) {
        if ( !this.checkTableBelongsToUser(tableId, principal) ) {
            throw new CannotAccessResourceException(String.format("The user %s cannot access table %d", principal.getName(), tableId)) ;
        }
        return this.tableEntityRepository.findTableEagerLevel1ById(tableId) ;
    }
    
    @Override
    @Transactional(isolation=Isolation.READ_UNCOMMITTED, readOnly=true)
    public Map<Object, Persistent> scan(Long tableId, String startKey, String endKey, Long limit) throws Exception {

      //TODO Add principal to parameters and check permissions
      
      Map<Object, Persistent> results = new LinkedHashMap<>() ;
        
        if (limit == null || limit < 2) {
            limit = 10L;
        }
        
        TableEntity table = this.tableEntityRepository.findById(tableId)
            .orElseThrow(
                () -> new ResourceNotFoundException("The table '" + tableId + "' was not found. Operation aborted."));
        
        // TODO This must be generalized to DataStore<?,Persistent> and Query<?,Persistent>
        @SuppressWarnings("unchecked")
        DataStore<String,Persistent> dataStore = (DataStore<String,Persistent>) this.dataStoreManager.getDataStore(table) ;        
        Query<String,Persistent> dataQuery = dataStore.newQuery() ;
        if (startKey != null && !startKey.equals("")) {
            dataQuery.setStartKey(startKey);
        }
        if (endKey != null && !endKey.equals("")) {
            dataQuery.setEndKey(endKey);
        }
        dataQuery.setLimit(limit);
        Result<?,Persistent> result = dataQuery.execute();
        
        while (result.next()) {
            results.put(result.getKey(), result.get().clone()) ;
        }
        
        return results;
    }

    @Override
    @Transactional(isolation=Isolation.READ_UNCOMMITTED)
    public TableEntity addNewTable(TableRequest tableRequest, Principal principal) {

        if (!this.connectionService.checkConnectionBelongsToUser(tableRequest.getConnection(), principal)) {
            throw new InternalServerErrorException("The related connection does not belong to the user '" + principal.getName()) ;  //500
        }
        
        Connection connection = this.connectionRepository.findById(tableRequest.getConnection()).get() ;
        TableEntity newTable = new TableEntity() ;
        BeanUtils.copyProperties(tableRequest, newTable, "connection") ;
        newTable.setConnection(connection);
        return this.tableEntityRepository.saveAndFlush(newTable) ;
    }

    @Override
    @Transactional(isolation=Isolation.READ_UNCOMMITTED)
    public TableEntity updateTable(Long tableId, JsonNode updateValues, Principal principal) {
        
        if ( !this.checkTableBelongsToUser(tableId, principal) ) {
            throw new CannotAccessResourceException(String.format("The user %s cannot access table %d", principal.getName(), tableId)) ;
        }
        
        TableEntity oldTable = this.tableEntityRepository.findTableEagerLevel1ById(tableId) ;
        ObjectReader updater = JSONObjectMapper.readerForUpdating(oldTable);
        TableEntity mergedTable = null ;
        try {
            mergedTable = updater.readValue(updateValues);
        } catch (IOException e) {
            e.printStackTrace();
        }
        TableEntity returnEntity = this.tableEntityRepository.saveAndFlush(mergedTable) ;
        this.dataStoreManager.removeTableFromCache(mergedTable) ;
        
        return returnEntity ;
    }

    @Override
    @Transactional(isolation=Isolation.READ_UNCOMMITTED)
    public void deleteTable(Long tableId, Principal principal) {
        if ( !this.checkTableBelongsToUser(tableId, principal) ) {
            throw new CannotAccessResourceException(String.format("The user %s cannot access table %d", principal.getName(), tableId)) ;
        }
        this.tableEntityRepository.deleteById(tableId) ;
        
    }

    @Override
    @Transactional(isolation=Isolation.READ_UNCOMMITTED)
    public PersistentBase createOrUpdateRow(Long tableId, String rowKey, ArrayNode rowDiff, Principal principal) throws IOException {
        if ( !this.checkTableBelongsToUser(tableId, principal) ) {
            throw new CannotAccessResourceException(String.format("The user %s cannot access table %d", principal.getName(), tableId)) ;
        }
        
        TableEntity table = this.tableEntityRepository.findById(tableId).get() ;
        // TODO This must be generalized to DataStore<?,Persistent> and Query<?,Persistent>
        @SuppressWarnings("unchecked")
        DataStore<String,Persistent> dataStore = (DataStore<String,Persistent>) this.dataStoreManager.getDataStore(table) ;        
        PersistentBase persistentEntity = (PersistentBase) dataStore.get(rowKey) ;
        if (persistentEntity == null) {
            persistentEntity = (PersistentBase) dataStore.newPersistent() ;
            persistentEntity.setDirty() ;
        }
        
        if (rowDiff != null) {
            try {
                DeepDiffHelper.merge(persistentEntity, rowDiff);
            } catch (GoraException | ClassNotFoundException e) {
                throw new InternalServerErrorException("Error creating some Persistent entity.", e) ;
            }
        }
        
        dataStore.put(rowKey, persistentEntity) ;
        dataStore.flush() ;
        return persistentEntity ;
    }
    
    @Override
    @Transactional(isolation=Isolation.READ_UNCOMMITTED, readOnly = true)
    public Boolean existsRow(Long tableId, String rowKey, Principal principal) throws IOException {
        if ( !this.checkTableBelongsToUser(tableId, principal) ) {
            throw new CannotAccessResourceException(String.format("The user %s cannot access table %d", principal.getName(), tableId)) ;
        }

        TableEntity table = this.tableEntityRepository.findById(tableId).get() ;
        // TODO This must be generalized to DataStore<?,Persistent> and Query<?,Persistent>
        @SuppressWarnings("unchecked")
        DataStore<String,Persistent> dataStore = (DataStore<String,Persistent>) this.dataStoreManager.getDataStore(table) ;        
        PersistentBase persistentEntity = (PersistentBase) dataStore.get(rowKey) ;
        if (persistentEntity == null) {
            return false ;
        }
        return true ;
    }
    
    @Override
    @Transactional(isolation=Isolation.READ_UNCOMMITTED)
    public void deleteRow(Long tableId, String rowKey, Principal principal) throws IOException {
        if ( !this.checkTableBelongsToUser(tableId, principal) ) {
            throw new CannotAccessResourceException(String.format("The user %s cannot access table %d", principal.getName(), tableId)) ;
        }
        
        TableEntity table = this.tableEntityRepository.findById(tableId).get() ;
        // TODO This must be generalized to DataStore<?,Persistent> and Query<?,Persistent>
        @SuppressWarnings("unchecked")
        DataStore<String,Persistent> dataStore = (DataStore<String,Persistent>) this.dataStoreManager.getDataStore(table) ;
        dataStore.delete(rowKey) ;
        dataStore.flush() ;
    }
    
    @Override
    @Transactional(isolation=Isolation.READ_UNCOMMITTED, readOnly=true)
    public boolean checkTableBelongsToUser(Long tableId, Principal principal) {
        boolean tableBelongsToUser = (null != this.tableEntityRepository.findByIdAndConnectionUserUsername(tableId, principal.getName())) ;
        this.em.clear(); // BUG from JPA not fetching again when asked for further levels of eagerness
        return tableBelongsToUser ;
    }
    
}
