/*******************************************************************************
 * Copyright 2018 Alfonso Nishikawa
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.nishilua.goraexplorer.tables;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.PreDestroy;
import javax.tools.Diagnostic;
import javax.tools.Diagnostic.Kind;
import javax.tools.DiagnosticCollector;
import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;

import org.apache.avro.Schema;
import org.apache.commons.io.FileUtils;
import org.apache.gora.compiler.GoraCompiler;
import org.apache.gora.persistency.Persistent;
import org.apache.gora.store.DataStore;
import org.apache.gora.store.DataStoreFactory;
import org.apache.hadoop.conf.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.nishilua.goraexplorer.exceptions.CompilationException;
import com.nishilua.goraexplorer.interceptors.DataStoreManagerClassLoader;

@Component("dataStoreManager")
public class DataStoreManager {
 
    Logger logger = LoggerFactory.getLogger(DataStoreManager.class);

    /**
     * HashMap: Long tableId -> DataStore<?> as DataStore cache 
     */
    private Map<Long, DataStore<?,Persistent>> dataStoreCache = new HashMap<>() ;
    
    /**
     * Returns the DataStore for the given tableEntity
     * @param tableEntity
     * @throws IOException 
     */
    public synchronized DataStore<?,Persistent> getDataStore(TableEntity tableEntity) throws IOException {
        if (this.dataStoreCache.get(tableEntity.getId()) == null) {
            this.addTableToDataStoreCache(tableEntity);
        }
        return this.dataStoreCache.get(tableEntity.getId()) ;
    }
    
    /**
     * Removes the DataStore for a cached table entity from the datastores cache
     * @param tableEntity - The table who's datastore will be cleared from the cache
     */
    public synchronized void removeTableFromCache(TableEntity tableEntity) {
        this.dataStoreCache.remove(tableEntity.getId()) ;
    }
    
    /**
     * Given a table entity, compiles the avro schema (the output directory belongs to the classpath from
     * {@link DataStoreManagerClassLoader}), creates a DataStore and adds the datastore to the cache
     * @param tableEntity - Table entity with configuration information
     * @throws IOException 
     */
    private void addTableToDataStoreCache(TableEntity tableEntity) throws IOException {
        
        String avroSchemaDefinition = tableEntity.getAvroSchema() ;
        // The following output directory is already in the classpath (in the thread's context class loader) thanks to the
        // Interceptor DataStoreManagerClassLoaderInterceptor
        File outputDirectory = new File("/tmp/goraexplorer/generated/") ;
        
        Schema compiledSchema = GoraCompiler.compileSchema(avroSchemaDefinition, outputDirectory, null) ;
        String persistentClassName = compiledSchema.getFullName() ;

        this.compileJavaFiles(outputDirectory);
        
        // Set the mapping information
        String xmlMappingDefinition = tableEntity.getMapping() ;
        Configuration hadoopConfiguration = DataStoreHelper.createConfigurationFromConnection(tableEntity.getConnection()) ;
        
        Properties goraProperties = DataStoreHelper.createPropertiesFromConnection(tableEntity.getConnection()) ;
        // Configure the mapping in the Properties
        goraProperties.setProperty("gora.mapping", xmlMappingDefinition) ;
        
        DataStore<?,Persistent> newDataStore = DataStoreFactory.getDataStore(
                tableEntity.getConnection().getType().getDataStoreClassName(),
                tableEntity.getKeyClass(),
                persistentClassName,
                goraProperties,
                hadoopConfiguration
                ) ;
        
        this.dataStoreCache.put(tableEntity.getId(), newDataStore) ;
    }
    
    private void compileJavaFiles(File sourceRootDirectory) throws IOException {
        logger.info("Compiling files from: {}", sourceRootDirectory);
        Collection<File> javaFiles = FileUtils.listFiles(sourceRootDirectory, new String[]{"java"}, true) ;
        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        StandardJavaFileManager fileManager = compiler.getStandardFileManager(null, null, null);
        DiagnosticCollector<JavaFileObject> diagnostics = new DiagnosticCollector<JavaFileObject>();
        Iterable<? extends JavaFileObject> compilationUnits = fileManager.getJavaFileObjectsFromFiles(javaFiles);
        
        ClassLoader cl = this.getClass().getClassLoader() ;
        URL[] urls = ((URLClassLoader)cl).getURLs();
        StringBuilder appClasspathBuilder = new StringBuilder() ;
        appClasspathBuilder.append(System.getProperty("java.class.path")) ;
        for (int i = 0 ; i < urls.length ; i++) {
            appClasspathBuilder.append(":") ;
            appClasspathBuilder.append(urls[i].getFile()) ;
        }
        
        List<String> compilationOptions = new ArrayList<>();
        compilationOptions.addAll(Arrays.asList("-classpath", appClasspathBuilder.toString()));
        compiler.getTask(null, fileManager, diagnostics, compilationOptions, null, compilationUnits).call();
        fileManager.close();
        
        boolean existsCompilationErrors = false ;
        for (Diagnostic<? extends JavaFileObject> diagnostic: diagnostics.getDiagnostics()) {
            if (diagnostic.getKind() == Kind.ERROR) {
                String errorMsg = String.format("    (Line %d) %s%n", diagnostic.getLineNumber(), diagnostic.getMessage(null)) ;
                logger.error(errorMsg);
                existsCompilationErrors = true ;
            } else {
                logger.warn(String.format("    (Line %d) %s", diagnostic.getLineNumber(), diagnostic.getMessage(null) ));                
            }
        }
        if (existsCompilationErrors) {
            throw new CompilationException("Failed compiling schema") ;
        }
        try {
            // Sometimes some built classes does not exist after successfuly been compiled.
            // Probably needs this sleep to let other threads work in writing, loading, etc... :\ only guessing
            Thread.sleep(100) ;
        } catch (InterruptedException e) {
            logger.warn("Sleep interrrupted!", e);
        }
    }

    @PreDestroy
    public synchronized void closeAll() {
        logger.debug("Closing datastores");
        for (DataStore<?,Persistent> dataStore: this.dataStoreCache.values()) {
            dataStore.close();
        }
        this.dataStoreCache.clear();
    }

}
