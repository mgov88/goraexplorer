/*******************************************************************************
 * Copyright 2018 Alfonso Nishikawa
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.nishilua.goraexplorer.tables;

import java.io.IOException;
import java.security.Principal;
import java.util.Collection;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.nishilua.goraexplorer.exceptions.CannotAccessResourceException;
import com.nishilua.goraexplorer.exceptions.InternalServerErrorException;
import com.nishilua.goraexplorer.exceptions.NotImplementedException;
import com.nishilua.goraexplorer.exceptions.WrongFilterParameterException;
import com.nishilua.goraexplorer.request.Filters;
import com.nishilua.goraexplorer.tables.request.ConnectionRequest;
import com.nishilua.goraexplorer.tables.response.BackendTablesMetadata;
import com.nishilua.goraexplorer.tables.response.ConnectionResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(value = "connections")
@ResponseStatus(HttpStatus.OK)
@RequestMapping(path = "/connections")
public class ConnectionsController {

    @Autowired
    ConnectionService connectionService;

    @RequestMapping(path = "/{connectionId}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('USER')")
    @ApiOperation(value = "Retrieves the given connection belonging to the user", response=Connection.class)
    @ApiResponses(value = { @ApiResponse(code = 403, message = "The user has not access to the connection") }) // Forbidden
    public @ResponseBody Connection getConnection(
            @ApiParam(value="Connection id") @PathVariable Long connectionId,
            @ApiParam(value="Spring's internal variable", access="internal", hidden=true)
            Principal principal)
    {
        if ( !this.connectionService.checkConnectionBelongsToUser(connectionId, principal) ) {
            throw new CannotAccessResourceException(String.format("The user %s cannot access connection %d", principal.getName(), connectionId)) ;
        }
        
        return this.connectionService.getConnection(connectionId, principal);
    }
    
    @RequestMapping(path = "/{connectionId}/metadata/tables", method = RequestMethod.GET)
    @PreAuthorize("hasRole('USER')")
    @ApiOperation(value = "Gets the tables' name from a native backend metadata", response=BackendTablesMetadata.class)
    @ApiResponses(value = { @ApiResponse(code = 403, message = "The user has not access to the connection"), // Forbidden
                            @ApiResponse(code = 500, message = "Error retreving the metadata"), // Internal Server Error
                            @ApiResponse(code = 501, message = "The backend does not support metadata retrieval")}) // Backend not supported
    public @ResponseBody BackendTablesMetadata getBackendTablesMetadata(
            @ApiParam(value="Connection id") @PathVariable Long connectionId,
            @ApiParam(value="Spring's internal variable", access="internal", hidden=true)
            Principal principal)
    {
        if ( !this.connectionService.checkConnectionBelongsToUser(connectionId, principal) ) {
            throw new CannotAccessResourceException(String.format("The user %s cannot access connection %d", principal.getName(), connectionId)) ;
        }
        
        try {
            return this.connectionService.getBackendTablesMetadata(connectionId, principal);
        } catch (ClassNotFoundException e) {
            throw new NotImplementedException("The backend does not support metadata retrieval",e) ;
        } catch (IOException e) {
            throw new InternalServerErrorException(e) ;
        }
    }

    @RequestMapping(path = "/{connectionId}/metadata/table/{tableName}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('USER')")
    @ApiOperation(value = "Gets a specific table metadata about it's fields", response=Object.class)
    @ApiResponses(value = { @ApiResponse(code = 403, message = "The user has not access to the connection"), // Forbidden
                            @ApiResponse(code = 500, message = "Error retreving the metadata"), // Internal Server Error
                            @ApiResponse(code = 501, message = "The backend does not support metadata retrieval")}) // Backend not supported
    public @ResponseBody Object getBackendTableMetadata(
            @ApiParam(value="Connection id") @PathVariable Long connectionId,
            @ApiParam(value="Backend table name") @PathVariable String tableName,
            @ApiParam(value="Spring's internal variable", access="internal", hidden=true)
            Principal principal)
    {
        if ( !this.connectionService.checkConnectionBelongsToUser(connectionId, principal) ) {
            throw new CannotAccessResourceException(String.format("The user %s cannot access connection %d", principal.getName(), connectionId)) ;
        }
        
        try {
            return this.connectionService.getBackendTableMetadata(connectionId, tableName, principal);
        } catch (ClassNotFoundException e) {
            throw new NotImplementedException("The backend does not support metadata retrieval",e) ;
        } catch (IOException e) {
            throw new InternalServerErrorException(e) ;
        }
    }
    
    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("hasRole('USER')")
    @ApiOperation(value = "Retrieves all connections belonging to the logged user", responseContainer="Set", response=Connection.class)
    @ApiResponses(value = { @ApiResponse(code = 403, message = "The user has not access to the connection"), // Forbidden
                            @ApiResponse(code = 400, message = "Wrong filter parameter") }) // Bad Parameter
    public @ResponseBody Collection<Connection> getConnections(
            @ApiParam(value="URL encoded JSON filter parameter: %5B%7B%22property%22%3A%22user%22%2C%22value%22%3A1%2C%22exactMatch%22%3Atrue%7D%5D")
                @RequestParam(name="filter", required=true) Filters filters,
            @ApiParam(value="Spring's internal variable", access="internal", hidden=true)
            Principal principal) throws Exception
    {
        if (filters.size() == 1) {
            if ( ! "user".equals(filters.get(0).getProperty()) )  {
                throw new WrongFilterParameterException("Expected filter for user id") ;
            }
            if ( ! principal.getName().equals(filters.get(0).getValue().toString())) {
                throw new WrongFilterParameterException("Cannot query other users' connections") ;
            }
        }
        return this.connectionService.getConnections(principal);
    }

    @RequestMapping(method = RequestMethod.POST)
    @PreAuthorize("hasRole('USER')")
    @ApiOperation(value = "Adds a new connection. The connection will be added to the logged user", response = Connection.class)
    @ApiResponses(value = { @ApiResponse(code = 500, message = "Error creating the new connection") }) // InternalServerError
    public @ResponseBody ConnectionResponse addNewConnection(
            @ApiParam(value="New connection data") @RequestBody ConnectionRequest newConnection,
            @ApiParam(value="Spring's internal variable", access="internal", hidden=true)
            Principal principal)
    {
        Connection createdConnection = this.connectionService.addNewConnection(newConnection, principal);
        ConnectionResponse newConnectionResponse = new ConnectionResponse() ;
        BeanUtils.copyProperties(createdConnection, newConnectionResponse);
        return newConnectionResponse ;
    }
    
    @RequestMapping(path="/{connectionId}", method = RequestMethod.PUT)
    @PreAuthorize("hasRole('USER')")
    @ApiOperation(value = "Updates a connection. The connection must belong to the logged user", response = Connection.class)
    @ApiResponses(value =
        { @ApiResponse(code = 403, message = "User has not access to the connection"), // Forbidden
          @ApiResponse(code = 500, message = "Error updating the connection") }) // InternalServerError
    public @ResponseBody ConnectionResponse updateConnection(
            @ApiParam(value="ID of the connection to update") @PathVariable("connectionId") Long connectionId,
            @ApiParam(value="Object with the update values to merge") @RequestBody JsonNode updateValues,
            @ApiParam(value="Spring's internal variable", access="internal", hidden=true)
            Principal principal)
    {
        if ( !this.connectionService.checkConnectionBelongsToUser(connectionId, principal) ) {
            throw new CannotAccessResourceException(String.format("The user %s cannot access connection %d", principal.getName(), connectionId)) ;
        }
        // Update the json values with the request ID (don't let pass different values)
        ((ObjectNode)updateValues).put("id", connectionId) ;
        // Do not let change user nor tables
        ((ObjectNode)updateValues).remove("user") ;
        ((ObjectNode)updateValues).remove("tables ") ;        
        Connection updatedConnection = this.connectionService.updateConnection(connectionId, updateValues, principal);
        ConnectionResponse connectionResponse = new ConnectionResponse() ;
        BeanUtils.copyProperties(updatedConnection, connectionResponse);
        
        return connectionResponse ;
    }
    
    @RequestMapping(path="/{connectionId}", method = RequestMethod.DELETE)
    @PreAuthorize("hasRole('USER')")
    @ApiOperation(value = "Deletes the given connection. The connection must belong to the logged user")
    @ApiResponses(value = { @ApiResponse(code = 500, message = "Error deleting the connection") }) // InternalServerError
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public @ResponseBody void deleteConnection(
            @ApiParam(value="ID of the connection to delete") @PathVariable("connectionId") Long connectionId,
            @ApiParam(value="Spring's internal variable", access="internal", hidden=true)
            Principal principal)
    {
        if ( !this.connectionService.checkConnectionBelongsToUser(connectionId, principal) ) {
            throw new CannotAccessResourceException(String.format("The user %s cannot access connection %d", principal.getName(), connectionId)) ;
        }
        this.connectionService.deleteConnection(connectionId, principal) ;
    }
    
}
