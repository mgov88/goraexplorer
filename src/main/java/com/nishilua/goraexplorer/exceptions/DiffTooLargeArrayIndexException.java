/*******************************************************************************
 * Copyright 2018 Alfonso Nishikawa
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.nishilua.goraexplorer.exceptions;

/**
 * This exception is for when in a Table Row Diff data (generated with deep-diff js), there is a referenced index in an array, but that index
 * is too large: in deep-diff there can be only indexes deleting elements (must exist), or indexes adding a new element at the end
 * of the array, where the index is just `num elements` (one element past the end). 
 * If the index is two elements or more past the end, this exception will be thrown.
 */
public class DiffTooLargeArrayIndexException extends RuntimeException {
    
    private static final long serialVersionUID = 8151544618103235652L;

    public DiffTooLargeArrayIndexException() {
        super();
    }

    public DiffTooLargeArrayIndexException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public DiffTooLargeArrayIndexException(String message, Throwable cause) {
        super(message, cause);
    }

    public DiffTooLargeArrayIndexException(String message) {
        super(message);
    }

    public DiffTooLargeArrayIndexException(Throwable cause) {
        super(cause);
    }

}
