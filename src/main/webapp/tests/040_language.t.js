
/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// WARNING:
// This tests do not work because the page reloads after a language change

// The solution with t.global.location.reload() doesn't work neither. https://www.bryntum.com/forum/viewtopic.php?f=20&t=6448&p=36540&hilit=location.reload#p36540

StartTest(function (t) {
    t.getExt().query = t.getExt().ComponentQuery.query ;
    t.describe('The application', function (t) {

        t.it('Must allow to change the language to spanish', function (t) {
            t.chain(
                {waitFor: 'CQVisible', args: 'login', desc: 'The Login window is visible'},
                {click: '>> button[name=language]', offset: ['100%-5','50%'], desc: 'Clicked on language button'},
                {click: '>> menuitem[text=Español]', desc: 'Selected spanish'},
                function(next) {
                    // Reload the page
                    t.global.location.reload() ;
                    next() ;
                },
                {waitFor: 'PageLoad'},
                {waitFor: 'CQVisible', args: 'login', desc: 'The Login window is visible'},
                function(next){
                    var loginButton = t.getExt().query('button[name=loginNext')[0] ;
                    t.expect(loginButton.getText()).toBe('Siguiente') ;
                    t.pass('Success changing language to spanish') ;
                }
            );
        });

        t.it('Must allow to change the language to spanish', function (t) {
            t.chain(
                {waitFor: 'CQVisible', args: 'login', desc: 'The Login window is visible'},
                {click: '>> button[name=language]', offset: ['100%-5','50%'], desc: 'Clicked on language button'},
                {click: '>> menuitem[text=English]', desc: 'Selected english'},
                {waitFor: 'PageLoad'},
                {waitFor: 'CQVisible', args: 'login', desc: 'The Login window is visible'},
                function(next){
                    var loginButton = t.getExt().query('button[name=loginNext')[0] ;
                    t.expect(loginButton.getText()).toBe('Next') ;
                    t.pass('Success changing language to english') ;
                }
            );
        });
    })
});