/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

describe('The application', function(t) {

    t.getExt().query = t.getExt().ComponentQuery.query ;

    t.it('Must log in as admin', function (t) {
        t.chain(
            {waitFor: 'CQVisible', args: 'login', desc: 'The Login window is visible'},
            {type: 'admin', target: '>> textfield[name=username]', clearExisting: true, desc: 'Typed username'},
            {type: 'admin', target: '>> textfield[name=password]', clearExisting: true, desc: 'Typed password'},
            {click: '>> button[text=' + _('Next') + ']', desc: 'Clicked Next'},
            {waitFor: 'CQNotVisible', args:'login', desc: 'The Login window hidden'}
        ) ;
    });

    var connectionsMenuOptionComponent = t.getExt().query('treelistitem[text="' + _('Connections') + '"]') ;

    t.it('Must create a new table', function(t) {
        t.chain(
            function(next) {
                // The navigation menu bar at left must be expanded in order for the tests to work :\
                t.getExt().query('app-main')[0].getController().expandNavigationSize() ;
                next() ;
            },
            {click: connectionsMenuOptionComponent[0].getEl(), desc: 'Clicked on Connection main menu option'},
            function(next) {
                // select the test connection row
                var connectionsGrid = t.getExt().query('connectionsgrid')[0],
                    connectionsStore = connectionsGrid.getStore(),
                    connectionModel = connectionsStore.getData().find('name', 'HBase @ localhost') ;

                if (connectionModel === null) {
                    t.fail('Expected to find a test connection with name \'HBase @ localhost\'. Breaking error. Execute with example data') ;
                } else {
                    var itemIndex = connectionsStore.indexOf(connectionModel) ;
                    var gridRow = t.getRow(connectionsGrid, itemIndex) ;
                    // Click the connection row
                    if (connectionsGrid.getSelectionModel().isSelected(connectionModel)) {
                        next() ;
                    } else {
                        // Row not selected
                        t.click(gridRow, next);
                    }
                }
            },

            // Delete old table if exists from previous tests execution
            function(next) {
                    var tablesGrid = t.getExt().query('tablesgrid')[0],
                        tablesStore = tablesGrid.getStore(),
                        tableModel = tablesStore.getData().find('name', 'NEW TABLE') ;

                    if (tableModel !== null) {
                        tablesStore.remove(tableModel) ;
                        tablesStore.sync(
                            GoraExplorer.applicationHelper.handleStoreSync({
                                successMessage: _('Test table deleted successfully'),
                                failureMessage: _('Error deleting the test table'),
                                success: function (batch, options) {
                                    next() ;
                                },
                                failure: function (batch, options) {
                                    t.fail('Could not delete an existing test table')
                                }
                            }));
                    } else {
                        // Nothing to delete
                        next();
                    }
            },

            {click: '>> button[text=' + _('Table') + ']', desc: 'Clicked on menu Table'},
            {click: '>> menuitem[text=' + _x('Table','New') + ']', desc: 'Clicked on new table option'},
            {type: 'NEW TABLE', target: '>> textfield[name=name]', clearExisting: true, desc: 'Typed new table name'},
            {type: 'NEW DESCRIPTION', target: '>> textfield[name=description]', clearExisting: true, desc: 'Typed new table description'},
            {
                type: '{}',
                target: '>> textarea[name=avroSchema]',
                desc: 'Typed Avro Schema content'
            },
            {
                type: '<?xml version="1.0"?>\n'+
                '<gora-odm>\n'+
                '</gora-odm>',
                target: '>> textarea[name=mapping]',
                desc: 'Typed Mapping content'
            },

            {click: '>> button[text=' + _('Create') + ']', desc: 'Clicked Create button'},
            function(next) {
                // Detect the new created table when there are no phantom models
                var tablesGrid = t.getExt().query('tablesgrid')[0],
                    tablesStore = tablesGrid.getStore() ;

                t.waitForFn( function() {
                    var tableModel = tablesStore.getData().find('name', 'NEW TABLE') ;
                    return (tableModel !== null) && !tableModel.phantom ;
                }, function () {
                    t.pass('New table detected') ;
                })
            }
        ) ;
    }) ;

    t.it('Must update a table', function(t) {
        t.chain(
            function(next) {
                // The navigation menu bar at left must be expanded in order for the tests to work :\
                t.getExt().query('app-main')[0].getController().expandNavigationSize() ;
                next() ;
            },
            {click: connectionsMenuOptionComponent[0].getEl(), desc: 'Clicked on Connection main menu option'},
            {waitForComponentVisible: 'connectionsgrid', desc: 'The connections grid is visible'},
            function(next) {
                // Find the test connection row
                var connectionsGrid = t.getExt().query('connectionsgrid')[0],
                    connectionsStore = connectionsGrid.getStore(),
                    connectionModel = connectionsStore.getData().find('name', 'HBase @ localhost') ;

                if (connectionModel === null) {
                    t.fail('Expected to find a test connection with name \'HBase @ localhost\'. Breaking error. Execute with example data') ;
                } else {
                    // Click the connection row if it is not already selected
                    var itemIndex = connectionsStore.indexOf(connectionModel) ;
                    var gridRow = t.getRow(connectionsGrid, itemIndex) ;
                    if (connectionsGrid.getSelectionModel().isSelected(connectionModel)) {
                        next() ;
                    } else {
                        // Row not selected
                        t.click(gridRow, next);
                    }
                }
            },
            function(next) {
                // Find the test table row
                var tablesGrid = t.getExt().query('tablesgrid')[0],
                    tablesStore = tablesGrid.getStore(),
                    tablesModel = tablesStore.getData().find('name', 'NEW TABLE') ;

                if (tablesModel === null) {
                    t.fail('Expected to find a test table with name \'NEW TABLE\'. Breaking error. Execute with example data') ;
                } else {
                    // Click the table row if it is not already selected
                    var itemIndex = tablesStore.indexOf(tablesModel) ;
                    var gridRow = t.getRow(tablesGrid, itemIndex) ;
                    if (tablesGrid.getSelectionModel().isSelected(tablesModel)) {
                        next() ;
                    } else {
                        // Row not selected
                        t.click(gridRow, next);
                    }
                }
            },
            {click: '>> button[text=' + _('Table') + ']', desc: 'Clicked on menu Table'},
            {click: '>> menuitem[text=' + _x('Table','Edit') + ']', desc: 'Clicked on edit table option'},

            {type: 'EDITED DESCRIPTION', target: '>> textfield[name=description]', clearExisting: true, desc: 'Typed a new description'},
            {
                type: 'Schema edited',
                target: '>> textarea[name=avroSchema]',
                desc: 'Typed Avro Schema content',
                clearExisting: true
            },
            {
                type: 'Mapping edited',
                target: '>> textarea[name=mapping]',
                desc: 'Typed Mapping content',
                clearExisting: true
            },

            {click: '>> button[text=' + _('Update') + ']', desc: 'Clicked Update button'},
            function(next) {
                // Wait until there are no records to update
                t.waitForFn(function(){
                    var tablesGrid = t.getExt().query('tablesgrid')[0],
                        tablesStore = tablesGrid.getStore() ;

                    // True if there are no more records to update
                    return tablesStore.getUpdatedRecords().length === 0 ;
                }, function() {
                    t.pass('Table updated') ;
                    next();
                }) ;
            },
            function(next) {
                // Check the expected data
                var tablesGrid = t.getExt().query('tablesgrid')[0],
                    tablesStore = tablesGrid.getStore()
                    tablesModel = tablesStore.getData().find('name', 'NEW TABLE') ;

                t.expect(tablesModel.get('description')).toBe('EDITED DESCRIPTION') ;
                t.expect(tablesModel.get('avroSchema')).toBe('Schema edited') ;
                t.expect(tablesModel.get('mapping')).toBe('Mapping edited') ;
                t.expect(tablesModel.dirty).toBeFalsy() ;
                t.expect(tablesModel.phantom).toBeFalsy() ;
            }
        ) ;
    }) ;

    t.it('Must delete a table', function(t) {
        t.chain(
            function(next) {
                // The navigation menu bar at left must be expanded in order for the tests to work :\
                t.getExt().query('app-main')[0].getController().expandNavigationSize() ;
                next() ;
            },
            {click: connectionsMenuOptionComponent[0].getEl(), desc: 'Clicked on Connection main menu option'},
            {waitForComponentVisible: 'connectionsgrid', desc: 'The connections grid is visible'},
            function(next) {
                // Find the test connection row
                var connectionsGrid = t.getExt().query('connectionsgrid')[0],
                    connectionsStore = connectionsGrid.getStore(),
                    connectionModel = connectionsStore.getData().find('name', 'HBase @ localhost') ;

                if (connectionModel === null) {
                    t.fail('Expected to find a test connection with name \'HBase @ localhost\'. Breaking error. Execute with example data') ;
                } else {
                    // Click the connection row if it is not already selected
                    var itemIndex = connectionsStore.indexOf(connectionModel) ;
                    var gridRow = t.getRow(connectionsGrid, itemIndex) ;
                    if (connectionsGrid.getSelectionModel().isSelected(connectionModel)) {
                        next() ;
                    } else {
                        // Row not selected
                        t.click(gridRow, next);
                    }
                }
            },
            function(next) {
                // Find the test table row
                var tablesGrid = t.getExt().query('tablesgrid')[0],
                    tablesStore = tablesGrid.getStore(),
                    tablesModel = tablesStore.getData().find('name', 'NEW TABLE') ;

                if (tablesModel === null) {
                    t.fail('Expected to find a test table with name \'NEW TABLE\'. Breaking error. Execute with example data') ;
                } else {
                    // Click the table row if it is not already selected
                    var itemIndex = tablesStore.indexOf(tablesModel) ;
                    var gridRow = t.getRow(tablesGrid, itemIndex) ;
                    if (tablesGrid.getSelectionModel().isSelected(tablesModel)) {
                        next() ;
                    } else {
                        // Row not selected
                        t.click(gridRow, next);
                    }
                }
            },
            {click: '>> button[text=' + _('Table') + ']', desc: 'Clicked on menu Table'},
            {click: '>> menuitem[text=' + _x('Table','Delete') + ']', desc: 'Clicked on delete table'},
            {click: '>> button[text=' + Ext.window.MessageBox.prototype.buttonText.ok + ']', desc: 'Clicked on OK'},
            function(next) {
                // Wait until the table has been deleted in the server
                t.waitForFn(function(){
                    var tablesGrid = t.getExt().query('tablesgrid')[0],
                        tablesStore = tablesGrid.getStore() ;

                    // True if removed records has been successfuly removed
                    return tablesStore.getRemovedRecords().length === 0 ;
                }, function() {
                    t.pass('Test table deleted') ;
                }) ;
            }
        ) ;
    }) ;

});