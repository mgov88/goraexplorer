/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

describe('The application', function(t) {

    t.it('Must log in as admin', function (t) {
        t.chain(
            {waitFor: 'CQVisible', args: 'login', desc: 'The Login window is visible'},
            {type: 'admin', target: '>> textfield[name=username]', clearExisting: true, desc: 'Typed username'},
            {type: 'admin', target: '>> textfield[name=password]', clearExisting: true, desc: 'Typed password'},
            {click: '>> button[text=' + _('Next') + ']', desc: 'Clicked Next'},
            {waitFor: 'CQNotVisible', args: 'login', desc: 'The Login window hidden'}
        );
    });

    t.monkeyTest(document.body, 40);
}) ;