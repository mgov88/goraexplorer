/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Helper class for unit tests. Declares methods that returns expected data for the tests.
 */
Ext.define('Tests.expect.UnitTests090', {
    singleton: true,

    getExpectedColumnsDefinition: function() {
        return [
            {
                "text":"baseUrl",
                "dataIndex":"baseUrl",
                "tooltip":"The original associated with this WebPage."
            },
            {
                "text":"status",
                "dataIndex":"status",
                "tooltip":"A crawl status associated with the WebPage, can be of value STATUS_UNFETCHED - WebPage was not fetched yet, STATUS_FETCHED - WebPage was successfully fetched, STATUS_GONE - WebPage no longer exists, STATUS_REDIR_TEMP - WebPage temporarily redirects to other page, STATUS_REDIR_PERM - WebPage permanently redirects to other page, STATUS_RETRY - Fetching unsuccessful, needs to be retried e.g. transient errors and STATUS_NOTMODIFIED - fetching successful - page is not modified"
            },
            {
                "text":"fetchTime",
                "dataIndex":"fetchTime",
                "tooltip":"The system time in milliseconds for when the page was fetched."
            },
            {
                "text":"prevFetchTime",
                "dataIndex":"prevFetchTime",
                "tooltip":"The system time in milliseconds for when the page was last fetched if it was previously fetched which can be used to calculate time delta within a fetching schedule implementation"
            },
            {
                "text":"score",
                "dataIndex":"score",
                "tooltip":"A score used to determine a WebPage's relevance within the web graph it is part of. This score may change over time based on graph characteristics."
            }
        ] ;
    }

}) ;