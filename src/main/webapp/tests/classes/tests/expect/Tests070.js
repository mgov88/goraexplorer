/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Helper class for tests. Declares methods that returns expected data for the tests.
 */
Ext.define('Tests.expect.Tests070', {
    singleton: true,

    getExpectedCreatedSchema : function () {
        return '{\n' +
            '  "type": "record",\n' +
            '  "namespace": "admin",\n' +
            '  "name": "Tweets",\n' +
            '  "fields": [\n' +
            '    {\n' +
            '      "name": "status",\n' +
            '      "default": "",\n' +
            '      "type": "string",\n' +
            '      "doc": "Text"\n' +
            '    },\n' +
            '    {\n' +
            '      "name": "latitud",\n' +
            '      "default": null,\n' +
            '      "type": [\n' +
            '        "null",\n' +
            '        "double"\n' +
            '      ],\n' +
            '      "doc": "Latitude"\n' +
            '    }\n' +
            '  ],\n' +
            '  "doc": "Tweets from Twitter"\n' +
            '}' ;
    },

    getExpectedCreatedMapping : function() {
        return '<?xml version="1.0" encoding="UTF-8"?>\n' +
            '<gora-odm>\n' +
            '  <table name="twitterraw">\n' +
            '    <family name="default" maxVersions="1"/>\n' +
            '  </table>\n' +
            '  <class table="twitterraw" keyClass="java.lang.String" name="admin.Tweets">\n' +
            '    <field name="status" family="default" qualifier="status"/>\n' +
            '    <field name="latitud" family="default" qualifier="latitud"/>\n' +
            '  </class>\n' +
            '</gora-odm>' ;
    },

    getExpectedUpdatedSchema: function() {
        return '{\n' +
            '  "type": "record",\n' +
            '  "namespace": "admin",\n' +
            '  "name": "Tweets",\n' +
            '  "fields": [\n' +
            '    {\n' +
            '      "name": "status",\n' +
            '      "default": "",\n' +
            '      "type": "string",\n' +
            '      "doc": "Text"\n' +
            '    },\n' +
            '    {\n' +
            '      "name": "latitud",\n' +
            '      "default": null,\n' +
            '      "type": [\n' +
            '        "null",\n' +
            '        "double"\n' +
            '      ],\n' +
            '      "doc": "Latitude"\n' +
            '    },\n' +
            '    {\n' +
            '      "name": "hashtags",\n' +
            '      "default": {},\n' +
            '      "type": {\n' +
            '        "type": "map",\n' +
            '        "values": {\n' +
            '          "type": "record",\n' +
            '          "namespace": "admin",\n' +
            '          "name": "Hashtag",\n' +
            '          "fields": [\n' +
            '            {\n' +
            '              "name": "texto",\n' +
            '              "default": "",\n' +
            '              "type": "string"\n' +
            '            },\n' +
            '            {\n' +
            '              "name": "inicio",\n' +
            '              "default": null,\n' +
            '              "type": [\n' +
            '                "null",\n' +
            '                "int"\n' +
            '              ]\n' +
            '            },\n' +
            '            {\n' +
            '              "name": "fin",\n' +
            '              "default": null,\n' +
            '              "type": [\n' +
            '                "null",\n' +
            '                "int"\n' +
            '              ]\n' +
            '            }\n' +
            '          ],\n' +
            '          "doc": "Hashtags"\n' +
            '        }\n' +
            '      }\n' +
            '    }\n' +
            '  ],\n' +
            '  "doc": "Tweets from Twitter"\n' +
            '}' ;
    },

    getExpectedUpdatedMapping: function() {
        return '<?xml version="1.0" encoding="UTF-8"?>\n' +
            '<gora-odm>\n' +
            '  <table name="twitterraw">\n' +
            '    <family name="default" maxVersions="1"/>\n' +
            '    <family name="hashtags" maxVersions="1"/>\n' +
            '  </table>\n' +
            '  <class table="twitterraw" keyClass="java.lang.String" name="admin.Tweets">\n' +
            '    <field name="status" family="default" qualifier="status"/>\n' +
            '    <field name="latitud" family="default" qualifier="latitud"/>\n' +
            '    <field name="hashtags" family="hashtags"/>\n' +
            '  </class>\n' +
            '</gora-odm>' ;
    }

}) ;