/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Given an EntityNode generates the grid columns definition.
 * Each field is a column, but the following fields are not shown:
 * <ul>
 *     <li>Bytes</li>
 *     <li>Map</li>
 *     <li>Array</li>
 *     <li>Record</li>
 * </ul>
 *
 * This visitor is implemented internally with a very internal side effects behaviour. Each method potentially
 * updates the private attributes <code>_columns</code>.
 */
Ext.define('GoraExplorer.view.tables.builders.avro.visitor.GridColumnsGeneratorVisitor', {
    extend: 'GoraExplorer.model.avro.visitor.AbstractSchemaVisitor',

    /**
     * Visiting columns definition
     * @private
     */
    _columns: null,

    /**
     * Given an EntityNode generats the grid columns definition.
     * @param {GoraExplorer.model.avro.EntityNode} node - The entity schema to generate the columns definition
     * @return {Object} the columns definition
     */
    generateColumns: function(node) {
        var me = this ;

        if ( !(node instanceof GoraExplorer.model.avro.EntityNode) ) {
            Ext.raise(_('GridColumnsGeneratorVisitor#generateColumns() can only be called with a instance of EntityNode')) ;
        }
        me._columns = [] ;

        node.accept(me) ;
        return me._columns ;
    },

    visitArrayNode: function(node) {
        // do nothing, can't be shown on the grid
    },

    visitBooleanNode: function(node) {
        var me = this ;
        me._genericVisitNode(node) ;
    },

    visitBytesNode: function(node) {
        // Do nothing. At this moment we can't convert the BASE64 to String
    },

    visitDoubleNode: function(node) {
        var me = this ;
        me._genericVisitNode(node) ;
    },

    visitEntityNode: function(node) {
        var me = this ;

        node.eachChild(function(childNode) {
            childNode.accept(me) ;
        }) ;
    },

    visitFloatNode: function(node) {
        var me = this ;
        me._genericVisitNode(node) ;
    },

    visitIntNode: function(node) {
        var me = this ;
        me._genericVisitNode(node) ;
    },

    visitLongNode: function(node) {
        var me = this ;
        me._genericVisitNode(node) ;
    },

    visitMapNode: function(node) {
        // do nothing, can't be shown on the grid
    },

    visitRecordNode: function(node) {
        // do nothing, can't be shown on the grid
    },

    visitStringNode: function(node) {
        var me = this ;
        me._genericVisitNode(node) ;
    },

    /**
     * Does a generic visit, since in most node types the operations are the same
     * @param {GoraExplorer.model.avro.FieldNode} node
     * @private
     */
    _genericVisitNode: function(node) {
        var me = this,
            nodeName = node.get('name'),
            nodeDoc = node.get('doc') ;

        me._columns.push({
            text: nodeName,
            dataIndex: nodeName,
            tooltip: nodeDoc
        }) ;

    }

}) ;