/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Generates the item config for a map field
 */
Ext.define('GoraExplorer.view.tables.builders.MapCardItemBuilder', {
    singleton: true,

    uses: [
        'GoraExplorer.view.tables.builders.ItemBuilderHelper',
        'GoraExplorer.view.tables.cards.ShowChildCardButton'
    ],

    /**
     * Generates the Values Card Item from a MapNode (of an avro schema tree node)
     *
     * @param {GoraExplorer.model.avro.MapNode} node - The map node to generate its item definition
     * @param {GoraExplorer.model.avro.EntitiesNode} tableSchemaEntitiesTree - The root of the entities schema tree
     * @param {String} currentBindPath
     * @return {Object} Item definition
     */
    generateItemDefinition: function(node, tableSchemaEntitiesTree, currentBindPath) {
        var me = this,
            nodeName = node.get('name'),
            nodeNullable = node.get('nullable'),
            nodeDoc = node.get('doc'),
            nodeValuesType = node.get('valuesType'),
            nodeValuesRecordName = node.get('valuesRecordName') ;

        if (nodeValuesType === 'record') {
            nodeValuesType = nodeValuesRecordName ;
        }

        var itemDefinition = {
            xtype : 'carditemcontainer',
            items: [
                {
                    xtype: 'valuescarditemlabel',
                    text: Ext.String.format ('{0} (map:{1}):', nodeName, nodeValuesType),
                    tooltip: nodeDoc
                },
                {
                    xtype: 'box',
                    flex: 1
                },
                {
                    xtype: 'show-childcard-button',
                    schemaNode: node,
                    bindPath: currentBindPath
                }
            ]
        };

        // Add null stuff
        Ext.Array.insert(
            itemDefinition.items,
            itemDefinition.items.length,
            GoraExplorer.itemBuilderHelper.createNullItemsDefinition(currentBindPath, node, nodeNullable, ['add', 'delete', 'null'])
        ) ;

        return itemDefinition ;
    }

},
function (MapCardItemBuilder){
    GoraExplorer.mapCardItemBuilder = MapCardItemBuilder ;
}) ;