/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Ext.define('GoraExplorer.view.tables.builders.ItemBuilderHelper', {
    singleton: true,

    /**
     * Builds the definition for the the null items buttons and checkbox in a CardItemContainer ({@link GoraExplorer.view.component.CardItemContainer})
     *
     * The buttons's types will depend on the value of `nodeNullable` and `enableOptions`.
     * If the node is not nullable, all buttons will be disabled always.
     * If the node is nullable, the buttons/checkbox to have behavior will be those configured in `enableOptions` as
     * an array or object:
     *
     * ```
     * ['add', 'delete', 'null']
     *
     * ```
     * or
     * ```
     * {
     *   add: true,
     *   delete: true,
     *   'null': true
     * }
     * ```
     *
     * The values at `currentBindPath` and `itemSchema` will be set for some of the buttons.
     *
     * @param {String} [currentBindPath] - String with the bind path associated with the button's element
     * @param {GoraExplorer.model.avro.SchemaNode} [itemSchema] - Array/Map schema when needed.
     * @param {Boolena} nodeNullable - If the definition of the node to generate is nullable
     * @param {Array/Object} enableOptions - Enables the proper buttons. Can be an array with values `'add'`, `'delete'`
     *            and `'null'`, or a hashmap with `'add'=>true`, `'delete'=>true`, `'null'=>true`.
     * @return {[]} The buttons definition
     */
    createNullItemsDefinition: function(currentBindPath, itemSchema, nodeNullable, enableOptions) {
        var addButtonXtype = 'fakeadditemvaluebutton',
            deleteButtonXtype = 'fakedeletevaluebutton',
            nullButtonXtpe = 'fakenullcheckboxindicator' ;

        if (nodeNullable) {
            if (Ext.isArray(enableOptions)) {
                if (Ext.Array.contains(enableOptions, 'add')) {
                    addButtonXtype = 'additemvaluebutton' ;
                }
                if (Ext.Array.contains(enableOptions, 'delete')) {
                    deleteButtonXtype = 'deletevaluebutton' ;
                }
                if (Ext.Array.contains(enableOptions, 'null')) {
                    nullButtonXtpe = 'nullcheckboxindicator';
                }
            }
            if (Ext.isObject(enableOptions)) {
                if (Ext.Object.getKey(enableOptions, 'add')){
                    addButtonXtype = 'additemvaluebutton' ;
                }
                if (Ext.Object.getKey(enableOptions, 'delete')) {
                    deleteButtonXtype = 'deletevaluebutton' ;
                }
                if (Ext.Object.getKey(enableOptions, 'null')) {
                    nullButtonXtpe = 'nullcheckboxindicator';
                }
            }
        }
        return [
            {
                xtype: 'box',
                width: 5
            },
            {
                xtype: 'container',
                cls: 'container-fake-buttons',
                items: [
                    {
                        xtype: addButtonXtype,
                        bindPath: currentBindPath,
                        itemSchema: itemSchema
                    },
                    {
                        xtype: deleteButtonXtype,
                        bindPath: currentBindPath
                    }
                ]
            },
            {
                xtype: nullButtonXtpe,
                bindPath: currentBindPath
            }
        ] ;
    }

},
function (ItemBuilderHelper){
    GoraExplorer.itemBuilderHelper = ItemBuilderHelper ;
}) ;