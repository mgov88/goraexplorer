/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * A readOnly checkbox that shows the null state of the "bindPath".
 */
Ext.define('GoraExplorer.view.tables.cards.NullCheckboxIndicator', {
    extend: 'Ext.form.field.Checkbox',

    alias: 'widget.nullcheckboxindicator',

    config: {
        /**
         * @cfg {String} bindPath - The bind path to show the null state
         */
        bindPath: null
    },

    /**
     * Applies the bindPath configuration to the ViewModel internally
     * @param config
     * @return {*|Object}
     */
    initConfig: function(config) {
        var me = this ;

        Ext.apply(config, {
            viewModel: {
                formulas: {
                    isNull: {
                        bind:  '{' + config.bindPath + '}'
                    }
                }
            }
        }) ;

        return me.callParent(arguments) ;
    },

    readOnly: true,
    padding: '0 0 0 5',
    boxLabel: 'Null',
    width: 55,
    bind: {
        value: '{isNull}'
    },
    viewModel: {
        formulas: {
            isNull: {
                bind: '', // Will be overwritten at #initComponent()
                get: function (value) {
                    return value === null;
                }
            }
        }
    }

}) ;