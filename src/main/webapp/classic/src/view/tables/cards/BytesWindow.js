/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Ext.define('GoraExplorer.view.tables.cards.BytesWindow', {
    extend: 'Ext.window.Window',

    alias: 'widget.byteswindow',

    requires: [
        'GoraExplorer.view.tables.cards.BytesWindowModel',
        'GoraExplorer.view.tables.cards.BytesWindowController'
    ],

    controller: 'byteswindow',
    viewModel: 'byteswindow',

    modal: true,

    layout: 'fit',

    glyph: 'xf15b@FontAwesome',
    maximizable: true,
    minHeight: '50%',
    minWidth: '50%',
    height: '50%',
    width: '50%',

    config: {
        /**
         * @cfg {String} bindPath - Bind path with the data of this field
         */
        bindPath: null
    },

    items: [
        {
            xtype: 'textarea',
            editable: false,
            bind: {
                value: '{formattedValue}',
                editable: '{editing}'
            }
        }

    ],

    dockedItems: [{
        xtype: 'toolbar',
        dock: 'top',
        items: [
            {
                xtype: 'combobox',
                reference: 'encodingCombobox',
                publishes: 'value',
                fieldLabel: _('Encoding'),
                forceSelection: true,
                editable: false,
                autoSelect: true,
                queryMode: 'local',
                value: 'Base64',
                store: [ 'Base64', 'UTF-8', 'ISO-8859' ]
            }
        ]
    }],

    buttons: [
        {
            text: _('Close'),
            listeners: {
                click: 'onCloseClick'
            }
        }
    ]

}) ;