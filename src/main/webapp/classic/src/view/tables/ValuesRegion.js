/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * The Values Region holds all the Values Card for a table row selected. It shows a breadcrumb common to all Values Card
 * and manages the consistency of the breadcrumbs with the selected card and loaded cards.
 */
Ext.define('GoraExplorer.view.tables.ValuesRegion', {
    extend: 'Ext.panel.Panel',

    uses: [
        'GoraExplorer.view.tables.ValuesRegionController',
        'GoraExplorer.view.tables.ValuesRegionModel',
        'GoraExplorer.helpers.toast.ToastHelper'
    ],

    requires: [
        'Ext.panel.Tool'
    ],

    alias: 'widget.valuesregion',

    controller: 'valuesregion',
    viewModel: 'valuesregion',

    layout: 'card',
    width: 400,
    minWidth: 400,

    split: true,
    collapsible: true,
    collapsed: true,
    title: _('Values'),
    cls: 'valuesregion',

    config: {

        /**
         * @cfg {GoraExplorer.view.tables.ValuesCard} selectedCard - Current selected card
         */
        selectedCard: null,

        /**
         * @cfg {String} selectedCardId - Current selected card id, usually md5(bindPath)
         */
        selectedCardId: null
    },

    bind: {
        title: '{regionTitle}',
        userCls: '{classOnEditing}'
    },

    tools: [
        {
            iconCls: 'x-fa fa-floppy-o',
            tooltip: _('Save changes'),
            bind: {
                hidden: '{!editing}'
            },
            handler: 'valuesRegionSaveEditClick'
        },
        {
            iconCls: 'x-fa fa-undo',
            tooltip: _('Discard changes'),
            bind: {
                hidden: '{!editing}'
            },
            handler: 'valuesRegionCancelEditClick'
        },
        {
            type: 'pencil',
            tooltip: _('Edit'),
            bind: {
                hidden: '{editingOrNoRowSelected}'
            },
            handler: 'valuesRegionEditClick'
        }
    ],

    initComponent: function() {
        var me = this ;

        me.dockedItems = [{
            xtype: 'toolbar',
            dock: 'top',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'breadcrumb',
                    reference: 'tableBreadcrumbs',
                    dock: 'top',
                    overflowHandler: 'scroller',
                    store: {
                        type: 'tree',
                        rootVisible: true
                    },
                    listeners: {
                        change: 'onBreadcrumbChange'
                    }
                }
            ]
        }];

        me.callParent(arguments) ;
    },

    listeners: {
        add: 'onAddValuesCard',
        beforeadd: 'onBeforeAddValuesCard',
        remove: 'onRemoveValuesCard',
        resize: 'onValuesRegionResize'
    },

    /**
     * Clears all the ValuesRegion
     */
    clear: function() {
        this.getController().clear() ;
    },

    /**
     * After updating the selected card, the card is set as the active card
     *
     * @param {GoraExplorer.view.tables.ValuesCard} newSelectedCard
     * @param {GoraExplorer.view.tables.ValuesCard} oldSelectedCard
     * @protected
     */
    updateSelectedCard: function(newSelectedCard, oldSelectedCard) {
        var me = this ;
        me._setSelectedValuesCard(newSelectedCard) ;
    },

    /**
     * When the selected card id changes, the corresponding card is shown
     * @param {String} newSelectedCardId - Usually md5(bindPath)
     * @param {String} oldSelectedCardId
     * @protected
     */
    updateSelectedCardId: function(newSelectedCardId, oldSelectedCardId) {
        var me = this ;
        var cardItem = me.child('component[cardId=' + newSelectedCardId +']') ;
        me._setSelectedValuesCard(cardItem) ;
    },

    /**
     * Shows the selected ValuesCard and updates the breadcrumb.
     *
     * @param {GoraExplorer.view.tables.ValuesCard} cardItem - Values Card to show
     * @private
     */
    _setSelectedValuesCard: function(cardItem) {
        var me = this,
            breadcrumbs = me.lookupReference('tableBreadcrumbs') ;

        if (Ext.isEmpty(cardItem)){
            return ;
        }

        var existingCardItem = me.child('component[cardId=' + cardItem.getCardId() +']') ;
        if (existingCardItem === null) {
            // The card item is new, so add it to the panel
            me.add(cardItem) ;
        }

        me.selectedCardId = cardItem.getCardId() ;
        me.selectedCard = cardItem ;

        me.setActiveItem(cardItem);
        if (cardItem.isFocusable(true)) {
            cardItem.focus();
        }

        if (breadcrumbs !== null) {
            var breadcrumbStore = breadcrumbs.getStore(),
                selectedNode = breadcrumbStore.getById(cardItem.getCardId()) ;
            //WORKAROUND start
            // https://www.sencha.com/forum/showthread.php?297088-Breadcrumb-No-arrow-Icon-after-dynamically-add-a-Node
            // not fixed at 6.2.0.981
            breadcrumbStore.suspendEvents(false) ;
            breadcrumbs.suspendEvents(false) ;
            var dummy = breadcrumbStore.getRoot().appendChild({id: 'dummy'}) ;
            dummy.suspendEvents(false) ;
            breadcrumbs.setSelection(dummy) ;
            dummy.remove() ;
            breadcrumbs.setSelection(selectedNode) ;
            breadcrumbs.resumeEvents(true) ;
            breadcrumbStore.resumeEvents(true) ;
            // Workaround end

        }
    },

    /**
     * Adds a new card to the values region if it does not exist.
     * To add a card, this method can be called, or the one at the controller {@ValuesCardController}.
     *
     * @param {Object} cardConfig - Configuration parameters
     * @param {String} [cardConfig.cardId] - The cardId of the new card. If not present, one will be autogenerated
     * @param {String} cardConfig.bindPath - The bind path to the data shown in the card, starting
     *        from 'recordData' as root and each key in the data structure is a token. Each token must be escaped
     *        with {@link Nishilua.String#escapeBind escapeBind}
     * @param {GoraExplorer.model.avro.SchemaNode} cardConfig.schemaNode - Avro type schema of the data in the card
     *
     * @return {GoraExplorer.view.tables.ValuesCard} The values card instance
     *
     * @public
     */
    addNewCard: function(cardConfig) {
        return this.getController().addNewCard(cardConfig) ;
    },

    /**
     * Given a cardId, returns the card in the values region or null.
     * @param {String} cardId
     * @returns {GoraExplorer.view.tables.ValuesCard | null}
     */
    getCard: function(cardId) {
        return this.child('component[cardId=' + cardId +']') ;
    },

    /**
     * Given a cardId, tells if the values region contains that card
     * @param {String} cardId
     * @returns {boolean}
     */
    containsCard: function(cardId) {
        return this.child('component[cardId=' + cardId +']') !== null ;
    },

    /**
     * Adds the ViewModel's variable `labelsWidth` to the state of the Values Region.
     * @return {Object}
     */
    getState: function () {
        var me = this,
            state = this.callParent(),
            labelsWidth = me.getViewModel().get('labelsWidth');

        if (Ext.isDefined(labelsWidth)) {
            state.labelsWidth = labelsWidth ;
        }

        return state;
    },

    /**
     * Loads the saved variable state `labelsWidth` into the ViewModel
     * @param {Object} newState
     */
    applyState: function(newState) {
        var me = this ;
        this.callParent(arguments) ;
        if (Ext.isDefined(newState) && Ext.isObject(newState)) {
            var labelsWidth = newState.labelsWidth ;
            if (Ext.isDefined(labelsWidth)) {
                me.getViewModel().set('labelsWidth', labelsWidth) ;
            }
        }
    }

});