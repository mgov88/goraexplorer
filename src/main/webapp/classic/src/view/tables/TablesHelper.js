/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Ext.define('GoraExplorer.view.tables.TablesHelper', {
    singleton: true,

    uses: [
        'GoraExplorer.view.tables.TableGrid',
        'GoraExplorer.view.tables.TableTab',
        'GoraExplorer.view.tables.TableTabModel',
        'GoraExplorer.view.tables.TableTabController',
        'GoraExplorer.view.tables.builders.avro.visitor.GridColumnsGeneratorVisitor'
    ],

    /**
     * Given a table ID, shows its Tab if the table is already opened, or creates a new tab for the table
     * @param {Number} tableId
     * @param {GoraExplorer.view.tables.TablesTabPanel} tablesTabPanel - TabPanel with the tables
     * @param {GoraExplorer.model.User} currentUser - Current user to access connections and tables information
     *
     * @returns {Ext.promise.Promise} Returns a promise whose 'then' function gets the created/shown card item
     */
    showOrCreateTableTab: function(tableId, tablesTabPanel) {
        var me = this,
            deferred = Ext.create('Ext.Deferred') ;

        var existingItems = tablesTabPanel.query('[tableId=' + tableId + ']',false),
            existingItem = existingItems[0];

        if (existingItems.length === 0) {

            // 1 - Load the table (by tableId) asynchronously
            GoraExplorer.model.Table.load(tableId,
                {
                    scope: me,
                    success: function(tableEntity, operation) {
                        // 2 - ViewModel
                        var tableTabModel = Ext.create('GoraExplorer.view.tables.TableTabModel', {
                            parent: tablesTabPanel.getViewModel(),
                            data: {
                                tableEntity: tableEntity
                            },
                            stores: {
                                tableStore: {
                                    proxy: {
                                        extraParams: { // Placeholder param for the url
                                            tableId: tableId
                                        }
                                    }
                                }
                            }
                        });

                        // 3 - Grid
                        tableTabModel.notify() ; // ViewModel has to compute all async scheduled NOW
                        var schemaEntitiesTree = tableTabModel.get('tableSchemaEntitiesTree'),
                            gridColumnsGenerator = Ext.create('GoraExplorer.view.tables.builders.avro.visitor.GridColumnsGeneratorVisitor') ;
                        // Grid added to tab in items

                        // 4 - Tab
                        existingItem = Ext.create('GoraExplorer.view.tables.TableTab',{
                            tableId: tableId,  // for existingItem search later
                            viewModel: tableTabModel,
                            tabConfig: {
                                title: tableTabModel.get('tableEntity.name'),
                                tooltip: tableTabModel.get('tableEntity.description')
                            },
                            items: [
                                {
                                    xtype: 'tablegrid',
                                    viewModel: { parent: tableTabModel },
                                    region: 'center',
                                    reference: 'tableGrid',
                                    publishes: ['atEnd', 'store'],
                                    columns: gridColumnsGenerator.generateColumns(schemaEntitiesTree.firstChild), // Columns from the first child (Main Entity)
                                    stateful: true,
                                    stateId: 'table-' + tableId
                                },
                                {
                                    xtype: 'valuesregion',
                                    viewModel: { type: 'valuesregion', parent: tableTabModel },
                                    region: 'east',
                                    reference: 'valuesRegion',
                                    stateful: true,
                                    stateId: 'valuesregion-' + tableId,
                                    weight: 100
                                },
                                {
                                    xtype: 'filtersregion',
                                    region: 'south',
                                    stateful: true,
                                    stateId: 'filtersregion-' + tableId,
                                    weight: 50
                                }
                            ]
                        });

                        tablesTabPanel.add(existingItem) ;
                        tablesTabPanel.setActiveTab(existingItem) ;

                        deferred.resolve(existingItem) ;
                    },
                    failure: function(tableEntity, operation) {
                        GoraExplorer.applicationHelper.handleFailedLoadResponse(operation, false) ;
                    }
                },
                tablesTabPanel.getSession()) ;

        } else {

            if (tablesTabPanel.getActiveTab() !== existingItem) {
                tablesTabPanel.setActiveTab(existingItem);
            }

            Ext.defer(deferred.resolve, 1, deferred, existingItem);
        }

        return deferred.promise ;
    }

}) ;