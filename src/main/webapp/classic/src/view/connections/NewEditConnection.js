/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Window used both to create a new connection, and for edit an existing connection.
 *
 * When creating the windows, must be configured with the parameters:
 *
 * ```
 * {
 *     isEdit: {Boolean} default false
 *     [, editConnectionEntity: {GoraExplorer.model.Connection}]
 * }
 * ```
 *
 * <ul>
 *     <li><code>isEdit: true</code> when the window will be used to edit an existing connection. In this case the
 *          parameter <code>editConnectionEntity</code> must hold the connection instance to be edited.</li>
 *     <li><code>isEdit: false</code> when the window will be used to create a new connection.</li>
 * </ul>
 *
 * This widget class internally self-configures a viewModel and <code>session: true</code>, so the updates
 * are done in a new session.
 */
Ext.define('GoraExplorer.view.connections.NewEditConnection', {
    extend: 'Ext.window.Window',
    alias: 'widget.new-edit-connection',

    requires: [
        'GoraExplorer.view.connections.NewEditConnectionController',
        'GoraExplorer.form.field.AppendFile'
    ],

    controller: 'new-edit-connection',

    session: true,

    /**
     * @cfg {Boolean} [isEdit=false] - Sets the component to be a "new connection" or "edit connection".
     *                         If the value is <code>true</code>, the component will be configured to edit an existing
     *                         connection (see {@link editConnection}. If the value is <code>false</code> the component will be configured
     *                         to create a new connection.
     */
    /**
     * @cfg {GoraExplorer.model.Connection} editConnectionEntity - When <code>isEdit === true</code>, the connection model to be edited
     *                                        must be configured here.
     */

    /**
     * Updates the configuration with the proper viewModel if does not have a viewModel.
     * This viewModel will hold the "isEdit" state  and the connection entity being created/updated.
     * @param {Object} [config] Config object.
     */
    constructor: function(config) {
        var me = this,
            defaultViewModel = null;

        if (config.isEdit) {
            // Edit connection
            if (Ext.isEmpty(config.editConnectionEntity)) {
                Ext.raise({msg: _('Missing configuration parameter editConnectionEntity when creating the widget NewEditConnection')}) ;
            }
            defaultViewModel = {
                data: {
                    isEdit: true
                },
                links: {
                    connection: config.editConnectionEntity
                }
            } ;
        } else {
            // New connection
            defaultViewModel = {
                data: {
                    isEdit: false
                },
                links: {
                    connection: {
                        type: 'GoraExplorer.model.Connection',
                        create: true
                    }
                }
            } ;
        }
        // By default the viewModel created here will be used, but can be overriden by the user
        var updatedConfig = Ext.merge(
            {
                viewModel: defaultViewModel
            },
            config
        );
        me.callParent([updatedConfig]);
    },

    bind: {
        title: '{isEdit:pick("' + _('New connection') + '","' + _('Edit connection') + '")}'
    },
    modal: true,
    maximizable: true,
    minWidth: 400,
    minHeight: 440,
    bodyPadding: 10,
    scrollable: true,
    layout: 'fit',

    buttons: [
        {
            name: 'createUpdate',
            bind: {
                text: '{isEdit:pick("' + _('Create') + '","' + _('Update') + '")}'
            },
            handler: 'onSaveClick'
        },
        {
            name: 'cancel',
            text: _('Cancel'),
            handler: 'onCancelClick'
        }
    ],

    defaults: {
        labelWidth: 150,
        modelValidation: true
    },
    items: [
        {
            xtype: 'form',
            reference: 'connectionForm',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'textfield',
                    name: 'name',
                    fieldLabel: _('Name'),
                    bind: '{connection.name}',
                    maxWidth: 550
                },
                {
                    xtype: 'textfield',
                    name: 'description',
                    fieldLabel: _('Description'),
                    bind: '{connection.description}'
                },
                {
                    xtype: 'combobox',
                    name: 'type',
                    fieldLabel: _('Type'),
                    bind: '{connection.type}',
                    forceSelection: true,
                    editable: false,
                    autoSelect: true,
                    queryMode: 'local',
                    value: 'HBASE',
                    store: [ 'HBASE', 'KUDU', 'IGNITE' ],
                    maxWidth: 300
                },
                {
                    xtype: 'container',
                    flex: 1,
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },
                    padding: '0 0 10 0', // Separate from the component bellow
                    items: [
                        {
                            xtype: 'textarea',
                            name: 'goraProperties',
                            fieldLabel: _('Gora Properties'),
                            bind: '{connection.goraProperties}',
                            reference: 'goraProperties',
                            flex: 1
                        },
                        {
                            xtype: 'appendfilefield',
                            name: 'goraPropertiesAppendFile',
                            appendTo: 'goraProperties' // reference name to the textarea to append the content of the file
                        }
                    ]
                },
                {
                    xtype: 'container',
                    flex: 1,
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },
                    padding: '0 0 10 0', // Separate from the component bellow
                    items: [
                        {
                            xtype: 'textarea',
                            name: 'hadoopConfiguration',
                            fieldLabel: _('Hadoop configuration'),
                            bind: '{connection.hadoopConfiguration}',
                            reference: 'hadoopConfiguration',
                            flex: 1
                        },
                        {
                            xtype: 'appendfilefield',
                            name: 'hadoopConfigurationAppendFile',
                            appendTo: 'hadoopConfiguration' // reference name to the textarea to append the content of the file
                        }
                    ]
                }
            ]
        }
    ]

});
