/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Wizard to create a 'new table wizad' window widget.
 */
Ext.define('GoraExplorer.view.connections.wizard.NewTable', {
    extend: 'Ext.window.Window',
    alias: 'widget.wizard-newtable',

    requires: [
        'GoraExplorer.view.connections.wizard.NewTableController',
        'GoraExplorer.view.connections.wizard.NewTableModel'
    ],
    uses: [
        'GoraExplorer.view.connections.wizard.NewTableMainInfo',
        'Ext.form.FieldContainer',
        'Ext.form.FieldSet'
    ],

    controller: 'wizard-newtable',
    viewModel: {
        type: 'wizard-newtable'
    },
    session: true,

    /**
     * @cfg {GoraExplorer.model.Connection} - Connection to which the new table will belong. Will
     * be copied to the viewModel on {@link NewTableController#initViewModel()}
     * @required
     * @readonly
     */
    parentConnection: null,

    /**
     * At init config, checks that a connection (the owner connection of the new table) is configured at creation time.
     * @override
     * @param instanceConfig
     */
    initConfig: function(instanceConfig) {
        var me = this ;

        Ext.applyIf(instanceConfig, {
            height: Ext.getBody().getViewSize().height * 0.8, // 80%
            width: Ext.getBody().getViewSize().width * 0.8 //80%
        }) ;

        if ( Ext.isEmpty(instanceConfig.parentConnection) ) {
            Ext.raise(_('GoraExplorer.view.connections.wizard.NewTable must have a parentConnection configured')) ;
        }
        this.callParent(arguments) ;
    },

    modal: true,
    layout: 'card',
    minHeight: 300,
    minWidth: 750,
    scrollable: true,
    maximizable: true,
    closable: true,
    resizable: false,
    bodyPadding: 10,
    title: _('New Table Wizard'),
    iconCls: 'x-fa fa-magic',

    items: [
    ],

    fbar: [
        {
            xtype: 'tbtext',
            text: _('Progress')
        },
        {
            xtype: 'progressbar',
            border: 1,
            minWidth: 160,
            bind: {
                value: '{progress}'
            }
        },
        '->',
        {
            xtype: 'button',
            text: _('Cancel'),
            handler: 'onCancelClick'
        },
        {
            xtype: 'button',
            text: _('Prev'),
            handler: 'onPrevClick',
            bind: {
                disabled: '{prevDisabled}'
            }
        },
        {
            xtype: 'button',
            handler: 'onNextClick',
            bind: {
               disabled: '{unsupportedBackend}',
               text: '{wizardState.isEndState:pick("' + _('Next') + '","' + _('Finish') + '")}'
            }
        }
    ],

    /**
     * Given a widget alias, searches the card and set it as active,
     * but if it does not exist it creates it first.
     *
     * @param {String} widgetAlias - Alias of the card to set active
     */
    setActiveItemOrCreate: function(widgetAlias) {
        var me = this ;

        // Find out if the card exists
        var cards = Ext.ComponentQuery.query(widgetAlias, me) ;
        if (cards.length !== 0) {
            // Found
            me.getLayout().setActiveItem(cards[0]) ;
        } else {
            // Not Found => create
            var newCard = Ext.widget(widgetAlias) ;
            me.add(newCard) ;
            me.getLayout().setActiveItem(newCard) ;
        }
    }

}) ;