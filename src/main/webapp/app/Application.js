/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * The main application class. An instance of this class is created by app.js when it
 * calls Ext.application(). This is the ideal place to handle application launch and
 * initialization details.
 */
Ext.define('GoraExplorer.Application', {
    extend: 'Ext.app.Application',
    
    name: 'GoraExplorer',

    requires: [
        // CLasses used in static context when instancing
        'GoraExplorer.helpers.ApplicationHelper',
        'GoraExplorer.store.NavigationTree'
    ],

    uses:[
        'GoraExplorer.helpers.toast.ToastHelper',
        'Ext.window.MessageBox',

        // Dependencies to have Tests deployed in production as example (for alpha)
        'Tests.UnitTestsData',
        'Tests.expect.TestsHelper',
        'Tests.expect.Tests070',
        'Tests.expect.UnitTests080',
        'Tests.expect.UnitTests090'
    ],

    // route default hash
    defaultToken: 'home',

    listen : {
        controller : {
            '#' : {
                unmatchedroute : 'onUnmatchedRoute'
            }
        }
    },

    onUnmatchedRoute : function(hash) {
        this.redirectTo('home') ;
    },

    /**
     * Reloads the application and sets the route to /login
     */
    reloadApplication: function() {
        var me = this ;

        var currentHref = window.location.href,
            hashIndex = currentHref.indexOf('#') ;

        if (hashIndex === -1 ) {
            // Hash not found -> Do a workaround to reload the page to the login route
            me.redirectTo("login", true) ;
        } else {
            // This other way shows a little less glitch since does not let events be processed
            var newHref = currentHref.substr(0, hashIndex) + '#login' ;
            window.location.href = newHref ;
        }
        window.location.reload() ;
    },

    /**
     * A template method that is called when your application boots.
     * It is called before the Ext.app.Applications launch function is executed so gives a hook point to run
     * any code before your Viewport is created.
     *
     * Configures the state.Manager
     *
     * @param {Ext.app.Application} application
     */
    init: function (application) {
        var me = this ;

        Ext.state.Manager.setProvider(new Ext.state.LocalStorageProvider()) ;

        Ext.Error.handle = function(err) {
            GoraExplorer.toastHelper.showGlobalErrorToast(err.msg) ;
            // Ext.log({level: 'error'}, err.msg) ;
            // return true;
        }

        // Load locale file
        var language = Ext.manifest.language,
            extJsLocaleFile = 'resources/i18n/locale/overrides/' + language + '/ext-locale-' + language +'.js' ;
        Ext.Loader.loadScript(extJsLocaleFile) ;

    },

    launch: function () {
        // Hide the custom Loader Indicator
        LoaderIndicator.finishedLoading() ;
    },

    onAppUpdate: function () {
        Ext.Msg.show({
            title: _('Application Update'),
            icon: Ext.Msg.QUESTION,
            message: _('This application has an update, reload?'),
            buttons: Ext.Msg.YESNO,
            closable: false,
            callback: function (choice) {
                if (choice === 'yes') {
                    window.location.reload();
                }
            },
            scope: this
        });
    }
});
