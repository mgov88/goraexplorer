/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Ext.define('GoraExplorer.helpers.toast.ToastHelper', {
    singleton: true,

    uses: [
        'Ext.window.Toast'
    ],

    /**
     * Shows a success messge
     * @param {String} message
     * @param {String} [title]
     */
    showSuccessToast: function(message, title) {
        Ext.toast({
            html: message,
            title: title || _('Success'),
            align: 'tr',
            iconCls: 'x-fa fa-check',
            alwaysOnTop: true,
            closable: true,
            minWidth: 250,
            autoCloseDelay: 5000
        }) ;
    },

    /**
     * Shows an info message
     * @param {String} message
     * @param {String} [title]
     */
    showInfoToast: function(message, title) {
        Ext.toast({
            html: message,
            title: title || _('Info'),
            align: 'tr',
            iconCls: 'x-fa fa-info',
            alwaysOnTop: true,
            closable: true,
            minWidth: 250,
            autoCloseDelay: 5000
        }) ;
    },

    /**
     * Shows an error message
     * @param {String} message
     * @param {String} [title]
     */
    showErrorToast: function(message, title) {
        Ext.toast({
            html: message,
            title: title || _('Error'),
            align: 'tr',
            iconCls: 'x-fa fa-minus-circle',
            alwaysOnTop: true,
            closable: true,
            minWidth: 250,
            autoCloseDelay: 5000
        }) ;
    },

    /**
     * Shows a warning message
     * @param {String} message
     * @param {String} [title]
     */
    showWarnToast: function(message, title) {
        Ext.toast({
            html: message,
            title: title || _('Warning'),
            align: 'tr',
            iconCls: 'x-fa fa-exclamation-triangle',
            alwaysOnTop: true,
            closable: true,
            minWidth: 250,
            autoCloseDelay: 5000
        }) ;
    },


    /**
     * Shows a globar error toast
     * @param {String} message
     */
    showGlobalErrorToast: function(message) {
        Ext.toast({
            html: message,
            title: _('Global error received'),
            align: 't',
            iconCls: 'x-fa fa-exclamation-triangle',
            alwaysOnTop: true,
            closable: true,
            minWidth: 300,
            maxWidth: 500,
            autoCloseDelay:10000
        }) ;
    }

},
function (ToastHelper){
    GoraExplorer.toastHelper = ToastHelper ;
}) ;