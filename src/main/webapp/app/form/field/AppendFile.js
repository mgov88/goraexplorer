/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * File field that must be configured with an "appendTo" property. The content of the selected file will
 * be append to the textarea with "name" equal to "appendTo".
 */
Ext.define('GoraExplorer.form.field.AppendFile', {
    extend: 'Ext.form.field.File',
    alias: 'widget.appendfilefield',

    requires: [
        'GoraExplorer.form.field.AppendFileController'
    ],

    controller: 'appendfilefield',

    /**
     * @cfg {String} appendTo
     * Reference name to the textarea to append the content of the file to.
     */
    appendTo: null,

    buttonText: _('Append file'),
    buttonOnly: true,
    buttonConfig: {
        tooltip: _('Adds the content of a file')
    },
    width: 110,
    listeners: {
        change: 'onInputFileChange'
    }

}) ;
