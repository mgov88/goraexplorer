/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Ext.define('GoraExplorer.view.tables.cards.BytesWindowController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.byteswindow',

    /**
     * Closes the window silently
     * @param button
     */
    onCloseClick: function(button) {
        this.getView().close() ;
    },

    /**
     * Sets the bindings with the config data
     */
    initViewModel: function (viewModel) {
        var me = this;
        viewModel.setFormulas({
            formattedValue: {
                bind: {
                    encoding: '{encodingCombobox.value}',
                    originalValue: '{' + me.getView().getBindPath() + '}'
                },
                get: function(bindValues){
                    switch (bindValues.encoding) {
                        case 'Base64':
                            return bindValues.originalValue ;
                            break ;
                        case 'UTF-8':
                            return Ext.util.Base64.decode(bindValues.originalValue) ;
                            break ;
                    }
                },
                set: function(formattedValue) {
                    var viewModel = this,
                        encoding = viewModel.get('encodingCombobox.value') ;

                    switch (encoding) {
                        case 'Base64':
                            viewModel.set(me.getView().getBindPath(), formattedValue);
                            break ;
                        case 'UTF-8':
                            viewModel.set(me.getView().getBindPath(), Ext.util.Base64.encode(formattedValue));
                            break ;
                    }
                }
            }
        }) ;
    }

}) ;