/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * ViewModel prototype for Table Tabs
 * Takes the configuration value at config.tableId and uses it as id to load a table data.
 */
Ext.define('GoraExplorer.view.tables.TableTabModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.tabletabviewmodel',

    requires: [
        'GoraExplorer.data.GoraStore'
    ],

    uses: [
        'GoraExplorer.model.avro.builder.SchemaTreeBuilder',
        'GoraExplorer.model.Table'
    ],

    data: {
        /**
         * {GoraExplorer.model.Table} Table entity holds the opened table. Must be loaded by hand in TablesHelper, since it is needed before
         * the grid is created because the property "columns" of the grid is required (and not bindable).
         */
        tableEntity: null
    },

    stores: {
        tableStore: {
            type: 'gorastore'
        }
    },

    formulas: {
        /**
         * Converts the avro schema in string format into a Schema Entities Tree
         */
        tableSchemaEntitiesTree: {
            bind: '{tableEntity}',
            get: function(tableEntity) {
                return GoraExplorer.schemaTreeBuilder.buildFromJSON(tableEntity.get('avroSchema')) ;
            }
        }
    }

}) ;