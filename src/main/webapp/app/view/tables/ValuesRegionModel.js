/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Ext.define('GoraExplorer.view.tables.ValuesRegionModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.valuesregion',

    data: {
        /**
         * Record of the selected row being shown in Values Region
         */
        recordData: null,

        /**
         * Holds the original `recordData` values when editing
         */
        originalRecordData: null,

        /**
         * Controls if the values of the row are being edited
         */
        editing: false,

        /**
         * Holds the configured labelsWidth for all the card items
         */
        labelsWidth: 125

    },

    formulas: {
        /**
         * The region title will show the record key when a record is selected in the table grid
         *
         * @param get
         * @returns {*}
         */
        regionTitle: function (get) {
            if (get('recordData') === null) {
                return _('Value') ;
            }
            return _('Values for ') + get('recordData.__id__') ;
        },

        /**
         * When editing, returns the class for ValuesRegion depending on if it is being edited.
         * @param get
         * @returns {String} 'editing' | ''
         */
        classOnEditing: function (get) {
            if (get('editing') === true) {
                return 'editing' ;
            }
            return '' ;
        },

        /**
         * Formula used to hide the ValuesRegion's edit button: is must be hidden when editing, or when a record is not selected
         * @param get
         * @return {boolean} true when editing ({editing}) or no row selected ({recordData})
         */
        editingOrNoRowSelected: function(get) {
            return get('editing') === true || Ext.isEmpty(get('recordData')) ;
        }

    }

}) ;