/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Ext.define('GoraExplorer.view.connections.wizard.NewTableSchemaModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.wizard-newtable-schema',

    uses: [
        'GoraExplorer.model.avro.ArrayNode',
        'GoraExplorer.model.avro.BooleanNode',
        'GoraExplorer.model.avro.BytesNode',
        'GoraExplorer.model.avro.DoubleNode',
        'GoraExplorer.model.avro.EntityNode',
        'GoraExplorer.model.avro.FloatNode',
        'GoraExplorer.model.avro.IntNode',
        'GoraExplorer.model.avro.LongNode',
        'GoraExplorer.model.avro.MapNode',
        'GoraExplorer.model.avro.RecordNode',
        'GoraExplorer.model.avro.SchemaNode',
        'GoraExplorer.model.avro.StringNode',
        'GoraExplorer.model.avro.FieldNode'
    ],

    formulas: {

        isField: function(get) {
            return get('entitiesTreePanel.selection') instanceof GoraExplorer.model.avro.FieldNode ;
        },

        // "isEntity" collides with ExtJS internal names :(
        selectedIsEntity: function(get) {
            return get('entitiesTreePanel.selection') instanceof GoraExplorer.model.avro.EntityNode ;
        },

        selectedIsMainEntity: function(get) {
            return get('selectedIsEntity') && get('entitiesTreePanel.selection').isFirst() ;
        },

        isBooleanField: function(get) {
            return get('entitiesTreePanel.selection') instanceof GoraExplorer.model.avro.BooleanNode ;
        },

        isIntField: function(get) {
            return get('entitiesTreePanel.selection') instanceof GoraExplorer.model.avro.IntNode ;
        },

        isLongField: function(get) {
            return get('entitiesTreePanel.selection') instanceof GoraExplorer.model.avro.LongNode ;
        },

        isFloatField: function(get) {
            return get('entitiesTreePanel.selection') instanceof GoraExplorer.model.avro.FloatNode ;
        },

        isDoubleField: function(get) {
            return get('entitiesTreePanel.selection') instanceof GoraExplorer.model.avro.DoubleNode ;
        },

        isStringField: function(get) {
            return get('entitiesTreePanel.selection') instanceof GoraExplorer.model.avro.StringNode ;
        },

        isArrayField: function(get) {
            return get('entitiesTreePanel.selection') instanceof GoraExplorer.model.avro.ArrayNode ;
        },

        isMapField: function(get) {
            return get('entitiesTreePanel.selection') instanceof GoraExplorer.model.avro.MapNode ;
        },

        isRecordField: function(get) {
            return get('entitiesTreePanel.selection') instanceof GoraExplorer.model.avro.RecordNode ;
        },

        /**
         * When defining an Array type, this flags when the items type of the array is Record
         * @param get
         * @return {boolean}
         */
        itemsTypeIsRecord: function(get) {
            return get('entitiesTreePanel.selection.itemsType') === 'record' ;
        },

        /**
         * when defining a Map type, this flags when the values type of the Map ir Record
         * @param get
         * @return {boolean}
         */
        valuesTypeIsRecord: function(get) {
            return get('entitiesTreePanel.selection.valuesType') === 'record' ;
        },

        /**
         * Returns true if the selected field is one of the fields that allows default value.
         * @param get
         * @return {Boolean} - true if the fields allows a default value
         */
        allowedDefaultValue: function(get) {
            return get('isBooleanField') || get('isIntField') || get('isLongField') ||
                   get('isFloatField') || get('isDoubleField') || get('isStringField') ||
                   get('isMapField') || get('isArrayField') ;
        },

        /**
         * The default value Input (or combobox) is disabled/enabled depending wheter a null value
         * is allowed or not, and if the null default value is checked or not.
         *
         * Given:
         *
         * a - Allow null value (true/false)
         * b - Set null as default value (true/false)
         *
         * The input must be enaled when: ¬a | (a & ¬b )
         * So, disabled when: ¬ ( ¬a | (a & ¬b ) )
         *   => a & ¬ (a & ¬b)
         *   => a & ( ¬a | b)
         *   => (a & ¬a) | (a & b)
         *   => F | (a & b)
         *   => a & b
         *
         * @param get
         * @return {*}
         */
        disableDefaultValueInput: function(get) {
            return get('entitiesTreePanel.selection.nullable') && get('entitiesTreePanel.selection.defaultNullValue') ;
        }

    },

    stores: {

        definedEntitiesStore: {
            source: '{avroSchemaStore}',
            filters: [
                function(item) {
                    return item instanceof GoraExplorer.model.avro.EntityNode ;
                }
            ]
        }

    }

}) ;