/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Controller of the second step of the Table Schema Wizard that is used to create let the user create the schema of
 * a table just clicking here and there.
 */
Ext.define('GoraExplorer.view.connections.wizard.NewTableSchemaController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.wizard-newtable-schema',

    requires: [
        'GoraExplorer.helpers.toast.ToastHelper'
    ],

    uses: [
        'GoraExplorer.model.avro.ArrayNode',
        'GoraExplorer.model.avro.BooleanNode',
        'GoraExplorer.model.avro.BytesNode',
        'GoraExplorer.model.avro.DoubleNode',
        'GoraExplorer.model.avro.EntityNode',
        'GoraExplorer.model.avro.FloatNode',
        'GoraExplorer.model.avro.IntNode',
        'GoraExplorer.model.avro.LongNode',
        'GoraExplorer.model.avro.MapNode',
        'GoraExplorer.model.avro.RecordNode',
        'GoraExplorer.model.avro.SchemaNode',
        'GoraExplorer.model.avro.StringNode'
    ],

    statics: {
        toastHelper: GoraExplorer.helpers.toast.ToastHelper
    },

    /**
     * Handles the click to add a new Entity. Simply adds a new entry tothe root in the schemas store
     */
    onAddEntity: function(button, e, eOpts) {
        var me = this,
            newRootNode = Ext.create('GoraExplorer.model.avro.EntityNode'),
            avroSchemaStore = me.getViewModel().get('avroSchemaStore') ;
        newRootNode.set('name', 'NewEntity') ;
        avroSchemaStore.getRoot().appendChild(newRootNode) ;
    },

    /**
     * Removes an entity, but only if it is not the first entity (because that is the main entity)
     * TODO Check if the entity being deleted is being used in a RecordNode before deleting it
     */
    onRemoveEntity: function(button, e, eOpts) {
        var me = this,
            entityToDelete = me.lookupReference('entitiesTreePanel').selection,
            avroSchemaStore = me.getViewModel().get('avroSchemaStore') ;

        if ( (entityToDelete instanceof GoraExplorer.model.avro.SchemaNode)
             && !(entityToDelete instanceof GoraExplorer.model.avro.EntityNode) )
        {
            // Delete a field node
            entityToDelete.remove() ;
            return ;
        }
        if (entityToDelete !== null && !entityToDelete.isFirst()) {
            avroSchemaStore.getRoot().removeChild(entityToDelete) ;
        } else {
            GoraExplorer.helpers.toast.ToastHelper.showInfoToast(_('The Main Entity cannot be deleted')) ;
        }
    },

    /**
     * Adds a boolean field to the selected MainEntity. If it is not a Entity ("main record"). Fails if it is not a Main Entity
     * @param button
     * @param e
     * @param {Object} eOpts
     * @param {String} eOpts.fieldType - field type name: 'Boolean', ...
     */
    onAddFieldClick: function(button, e, eOpts) {
        var me = this,
            entitiesTreepanel = me.lookupReference('entitiesTreePanel'),
            selectedEntity = entitiesTreepanel.selection,
            fieldType = eOpts.fieldType ;

        // If the selected node is a field node, add the new field to the parent node.
        if ( !(selectedEntity instanceof GoraExplorer.model.avro.EntityNode) ) {
            selectedEntity = selectedEntity.parentNode;
        }

        var newFieldNode = Ext.create('GoraExplorer.model.avro.' + fieldType + 'Node') ;
        newFieldNode.set('name', 'new' + fieldType + 'Field') ;
        selectedEntity.appendChild(newFieldNode) ;
        selectedEntity.expand(true) ;
    },

    /**
     * Deletes the selected field
     * @param button
     * @param e
     * @param eOpts
     */
    onDeleteFieldClick: function(button, e, eOpts) {
        var me = this,
            entitiesTreepanel = me.lookupReference('entitiesTreePanel'),
            selectedEntity = entitiesTreepanel.selection ;

        if (selectedEntity instanceof GoraExplorer.model.avro.EntityNode) {
            GoraExplorer.toastHelper.showInfoToast(_('Cannot remove an Entity with this menu option')) ;
            return ;
        }

        selectedEntity.remove() ;
    }

}) ;