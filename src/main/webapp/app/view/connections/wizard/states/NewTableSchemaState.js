/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * HBase especific state for a new table, to create the schema and mapping
 */
Ext.define('GoraExplorer.view.connections.wizard.states.NewTableSchemaState', {
    extend: 'GoraExplorer.view.connections.wizard.states.AbstractNewTableState',

    uses: [
        'GoraExplorer.view.connections.wizard.NewTableSchema',
        'GoraExplorer.model.avro.EntityNode',
        'GoraExplorer.view.connections.wizard.states.NewTableHBASEState'
    ],

    /*
     * Camelization expression and function
     */
    _camelRe: /(\s[a-zA-Z])/gi,
    _camelReplaceFn: function(m, a) { return a.charAt(1).toUpperCase(); },

    onEnterState: function(wizardView, previousState) {
        Ext.log({level: 'debug'}, 'Entered Schema State') ;

        var me = this,
            viewModel = wizardView.getViewModel(),
            avroSchemaStore = viewModel.get('avroSchemaStore') ;

        Ext.suspendLayouts() ;
        // Set step
        viewModel.set('wizardStep', 1) ;

        // Create the Main Entity if it does not exists
        if (avroSchemaStore.getRoot().childNodes.length === 0) {
            var newRootNode = Ext.create('GoraExplorer.model.avro.EntityNode'),
                safeEntityName = Ext.String.capitalize(viewModel.get('table.name')) ;

            safeEntityName = safeEntityName.trim() ;
            safeEntityName = safeEntityName.replace(me._camelRe, me._camelReplaceFn) ;
            safeEntityName = safeEntityName.replace(/[^a-zA-Z_]/g, '') ;

            newRootNode.set('name', safeEntityName) ;
            newRootNode.set('doc', viewModel.get('table.description')) ;
            avroSchemaStore.getRoot().appendChild(newRootNode) ;
        }

        wizardView.setActiveItemOrCreate('wizard-newtable-schema') ;
        wizardView.center() ;
        Ext.resumeLayouts(true) ;
    },

    prevState: function(wizardView) {
        // Transition to MainInfo card
        var me = this,
            viewModel = wizardView.getViewModel() ;

        var nextState = Ext.create('GoraExplorer.view.connections.wizard.states.NewTableInitialStepState') ;
        viewModel.setState(nextState) ;
    },

    /**
     * Performs the operation of wanting to transition to the next state, that is dependen on the native datastore
     * If all is correct, transitions to the backend-specific state
     * @param {GoraExplorer.view.connections.wizard.NewTable} wizardView - The affected instance
     */
    nextState: function(wizardView) {
        var me = this,
            viewModel = wizardView.getViewModel(),
            backendType = viewModel.get('backendMetadata').get('type') ;

        // TODO CHECK some constraints in the definitions (if any, just a todo)

        // Transition to widget backend-dependent
        var nextState = Ext.create('GoraExplorer.view.connections.wizard.states.NewTable' + backendType + 'State') ;
        viewModel.setState(nextState) ;
    }

}) ;