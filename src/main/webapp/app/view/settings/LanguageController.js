/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Ext.define('GoraExplorer.view.settings.LanguageController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.language',

    /**
     * The init function is called after initComponent, before render.
     * At this moment, the language option at the localStorage must be configured to be shown in the split button.
     */
    init: function() {
        var language = localStorage ? (localStorage.getItem('user-language') || 'en') : 'en',
            buttonView = this.getView(),
            buttonText = 'English';

        buttonView.setIconCls(language);
        var currentLanguageMenuItem = buttonView.getMenu().items.findBy(
                function ( item, key ) {
                    return item.iconCls === language ;
                }
            ) ;

        if (currentLanguageMenuItem !== null) {
            buttonText = currentLanguageMenuItem.text ;
        }
        buttonView.setText(buttonText) ;
    },

    /**
     * Handles the selection of a language in the Language split button. Shows the selected language.
     * Sets in the local Storage the 'user-language' key with the iconCls selected
     * @param item - Selected menu option (language)
     * @param e
     * @param options
     */
    onMenuItemClick: function(item, e, options){
        var menu = this.getView() ;
        menu.setIconCls(item.iconCls) ;
        menu.setText(item.text) ;
        localStorage.setItem("user-language", item.iconCls) ;
        GoraExplorer.getApplication().reloadApplication() ;
    }

}) ;