/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 */
Ext.define('GoraExplorer.view.main.MainController', {
    extend: 'GoraExplorer.view.ViewController',

    alias: 'controller.main',

    mixins: [
        'GoraExplorer.view.main.mixin.MainControllerService'
    ],

    uses: [
        'GoraExplorer.view.home.Home',
        'GoraExplorer.view.connections.Connections',
        'GoraExplorer.view.login.Login',
        'GoraExplorer.view.main.UserMenu',
        'GoraExplorer.view.settings.Settings',
        'GoraExplorer.view.tables.Tables',
        'GoraExplorer.view.tables.TablesHelper'
    ],
    
    routes: {
        home:                 { before: 'loginBeforeAction', action: 'onHome' },
        login:                'onLogin',
        connections:          { before: 'loginBeforeAction', action: 'onConnections' },
        tables:               { before: 'loginBeforeAction', action: 'onTables'},
        'table/:id' :         { before: 'loginBeforeAction', action: 'onTableId'},
        settings:             { before: 'loginBeforeAction', action: 'onSettings'}
    },

    /**
     * Route /home handler:
     */
    onHome: function() {
        var me = this;
        me.setCurrentView('home') ;
    },

    /**
     * Route /login handler: shows the login window
     */
    onLogin: function() {
        var me = this ;
        if (me.loginWindow == null) {
            me.loginWindow = Ext.widget('login') ;
        }
        me.loginWindow.show() ;
    },

    /**
     * Route /connections handler: shows the connections and their tables
     */
    onConnections: function() {
        var me = this;
        me.setCurrentView('connections') ;
    },

    /**
     * Route /tables handler: shows the section of tables
     */
    onTables: function() {
        var me = this ;
        me.setCurrentView('tables') ;
    },

    /**
     * Route /table/:id handler: opens the tab for the given table in "Tables" section (new if does not exists)
     * @param {Number} tableId - ID of the table to open/view
     */
    onTableId: function(tableId) {
        var me = this ;
        me.selectTable(tableId) ;
    },

    /**
     * Route /settings handler: shows the settings section (but no on main menu)
     */
    onSettings: function() {
        var me = this;
        me.setCurrentView('settings');
    },

    /**
     * When a navigation menu option is clicked the selection changes, and this event handler redirects to the "routeId" of the model.
     *
     * @param tree
     * @param [node=null] - New node selected. It's routedId will be used as the new hashtag to route. If null, nothing happens.
     */
    onNavigationTreeSelectionChange: function (tree, node) {
        if (node !== null) {
            this.redirectTo(node.get('routeId'));
        }
    },

    /**
     * Event handler when the user clicks on the profile image to show the user menu
     */
    profileImageClick: function() {
        var me = this;
        if (me.currentUserToolbarImageMenu === null) {
            me.currentUserToolbarImageMenu = Ext.widget('currentusertoolbarimagemenu');
        }
        me.currentUserToolbarImageMenu.showBy(me.lookupReference('currentUserToolbarImage')) ;
    },

    /**
     * Copied from admin template
     * @param hashTag - URL hash
     */
    setCurrentView: function(hashTag) {
        hashTag = (hashTag || '').toLowerCase();

        var me = this,
            refs = me.getReferences(),
            mainCard = refs.mainCardPanel,
            mainLayout = mainCard.getLayout(),
            navigationList = refs.navigationTreeList,
            store = navigationList.getStore(),
            node = store.findNode('routeId', hashTag),
            view = (node === null) ? hashTag : node.get('viewType'),
            lastView = me.lastView,
            existingItem = mainCard.child('component[routeId=' + hashTag + ']'),
            newView;

        // Kill any previously routed window
        if (lastView && lastView.isWindow) {
            lastView.destroy();
        }

        lastView = mainLayout.getActiveItem();

        if (!existingItem) {
            newView = Ext.create({
                xtype: view,
                routeId: hashTag,  // for existingItem search later
                hideMode: 'offsets'
            });
        }

        if (!newView || !newView.isWindow) {
            // !newView means we have an existing view, but if the newView isWindow
            // we don't add it to the card layout.
            if (existingItem) {
                // We don't have a newView, so activate the existing view.
                if (existingItem !== lastView) {
                    mainLayout.setActiveItem(existingItem);
                }
                newView = existingItem;
            }
            else {
                // newView is set (did not exist already), so add it and make it the
                // activeItem.
                Ext.suspendLayouts();
                mainLayout.setActiveItem(mainCard.add(newView));
                Ext.resumeLayouts(true);
            }
        }

        if (node === null) {
            //The node does not exists in the main menu
            navigationList.setSelection(null) ;
        } else {
            navigationList.setSelection(node); // This throws selectionchange event handled by Main to change route
        }

        if (newView.isFocusable(true)) {
            newView.focus();
        }

        me.lastView = newView;
    },

    /**
     * Handles the change of navigation menu size when burguer icon is clicked
     */
    onToggleNavigationSize: function () {
        var me = this,
            refs = me.getReferences(),
            navigationList = refs.navigationTreeList,
            wrapContainer = refs.mainContainerWrap,
            collapsing = !navigationList.getMicro(),
            new_width = collapsing ? 64 : 250;

        if (!collapsing) {
            navigationList.setMicro(false);
            // Save burger state
            Ext.state.Manager.set(me.BURGER_STATE_MICRO_KEY, false) ;
        }

        refs.logo.animate({dynamic: true, to: {width: new_width}});
        navigationList.width = new_width;
        wrapContainer.updateLayout({isRoot: true});

        if (collapsing) {
            navigationList.on({
                afterlayoutanimation: function () {
                    navigationList.setMicro(true);
                    // Save burger state
                    Ext.state.Manager.set(me.BURGER_STATE_MICRO_KEY, true) ;
                },
                single: true
            });
        }
    },

    /**
     * If the navigation bar size is collapsed, it expands it like if the burguer was pressed
     *
     * - Toggles the burger only if it is collapsed
     */
    expandNavigationSize: function() {
        var me = this,
            refs = me.getReferences(),
            navigationList = refs.navigationTreeList,
            microState = navigationList.getMicro() || false ;

        if (microState === true) {
            Ext.Function.createDelayed(me.onToggleNavigationSize, 1000, me)();
        }
    },

    /**
     * Performs:
     *
     * - Toggle burger if needed
     */
    onAfterLayout: function() {
        var me = this,
            refs = me.getReferences(),
            navigationList = refs.navigationTreeList,
            microState = navigationList.getMicro() || false ;

        if (Ext.state.Manager.get(me.BURGER_STATE_MICRO_KEY) != microState) {
            Ext.Function.createDelayed(me.onToggleNavigationSize, 1000, me)();
        }
    },

    privates: {
        /**
         * Refence to the singleton login window
         * @private
         */
        loginWindow: null,

        /**
         * Reference to the profile floating menu when clicking on the user icon at the toolbar
         */
        currentUserToolbarImageMenu: null
    },

    statics: {
        /**
         * State used for the collapsing/expanding burguer
         */
        BURGER_STATE_MICRO_KEY: 'BURGER_STATE_MICRO_KEY'
    }
});
