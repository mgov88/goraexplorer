/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * This model represents a single row in a table.
 * This model is used only to hold rows at GoraStore and to create new entites. To update entities GoraPersistentDiff must be used.
 */
Ext.define('GoraExplorer.model.GoraPersistent', {
    extend: 'Ext.data.Model',

    idProperty: '__id__',
    fields: [{name: '__id__'}],

    proxy: {
        type: 'rest',
        appendId: false,
        withCredentials: true,
        api: {
            create: GoraExplorer.applicationHelper.getRestUrl('tables/tableId/${tableId}/row'),
            destroy: GoraExplorer.applicationHelper.getRestUrl('tables/tableId/${tableId}/row')
        }
    }

}) ;