/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Model that holds a diff when updating a table row. Used to call POST /tables/row/${tableId}
 * Must not be reused, only created, used and discarded.
 *
 * The `rowDiff` field is the result of the deep-diff (/resources/js/deep-diff-....min.js)
 *
 * `rowKey` field is the field with the `__id__` value
 */
Ext.define('GoraExplorer.model.GoraPersistentDiff', {
    extend: 'Ext.data.Model',

    fields: [
        { name: 'rowDiff', type: 'auto' },
        { name: 'rowKey', type: 'string'}
    ],

    proxy: {
        type: 'rest',
        appendId: false,
        withCredentials: true,

        actionMethods: {
            create : 'PUT' // On creation, just do a PUT, because semantically it is an update
        },
        api: {
            create: GoraExplorer.applicationHelper.getRestUrl('tables/tableId/${tableId}/row')
        }
    }

});
