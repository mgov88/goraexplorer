/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Ext.define('GoraExplorer.model.User', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.validator.Email',
        'Ext.data.validator.Inclusion',
        'Ext.data.validator.Length',
        'GoraExplorer.helpers.ApplicationHelper',
        'GoraExplorer.model.Group'
    ],

    uses: [
        'GoraExplorer.model.Connection'
    ],

    idProperty: 'username',

    manyToMany: {
        UserGroup: {
            type: 'GoraExplorer.model.Group',
            role: 'groups',
            field: 'id',
            right: {
                role: 'users',
                field: 'username'
            }
        }
    },

    fields: [
        { name: 'username', type: 'string' },
        { name: 'email'   , type: 'string', defaultValue: '' },
        { name: 'imageUrl', type: 'string',
            convert: function (v) { return !Ext.isEmpty(v) ? v : 'resources/images/user-profile/noImage.png'; }
        },
        { name: 'enabled' , type: 'boolean', defaultValue: false }
    ],

    validators: {
        username: [
            {type: 'presence'},
            {type: 'length', min: 5, max: 50}
        ],
        email: [
            {type: 'presence'},
            {type: 'email'}
        ],
        enabled: [
            {type: 'presence'},
            {type: 'inclusion', list: [true, false]}
        ]
    },

    proxy: {
        type: 'rest',
        url: GoraExplorer.applicationHelper.getRestUrl('users/userMainData'),
        appendId: false,
        withCredentials: true
    },

    /**
     * @return {String[]} - Array of authority permissions that the user has
     */
    getAuthorities: function() {
        var me = this ;
        var authorities = {} ;
        me.groups().each(function(group) {
            group.authorities().each(function(authority){
                authorities[authority.get('authority')] = true ; // Add to set
            }) ;
        }) ;
        return Object.keys(authorities) ;
    },

    /**
     * Given a role name (for example "ROLE_ADMIN"), tells if the user has that authority permission
     * @return {Boolean} - True if the user has certain role
     */
    hasRole: function(roleName) {
        var me = this ;
        return me.getAuthorities().indexOf(roleName) !== -1 ;
    }

});
