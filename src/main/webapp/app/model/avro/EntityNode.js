/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * An entity is a record node, but we have to diferenciate the record definition from the field/value that is a record
 */
Ext.define('GoraExplorer.model.avro.EntityNode', {
    extend: 'GoraExplorer.model.avro.SchemaNode',

    requires: [
        'Ext.data.validator.Format',
        'Ext.data.validator.Presence'
    ],

    fields: [

        /**
         * @override
         * @readonly
         */
        { name: 'type', type: 'string', defaultValue: 'entity' },

        /**
         * @field {String[]} alias - Array of names as alias
         */
        { name: 'aliases', type: 'auto' },

        // NodeInterface related values -----------------------------------------

        /**
         * @override
         * @readonly
         */
        { name: 'iconCls', type: 'string', defaultValue: 'x-fa fa-address-card-o' },

        /**
         * @override
         * @readonly
         */
        { name: 'leaf', type: 'boolean', defaultValue: false}

    ],

    validators: {
        name: [
            { type: 'presence' },
            { type: 'format', matcher: /^[A-Z][a-zA-Z_]*$/, message: _('The Entity name must begin with a capital letter and contain only letters, numbers and underscores') }
        ]
    },

    /**
     * @override
     */
    accept: function(visitor) {
        return visitor.visitEntityNode(this) ;
    }

}) ;