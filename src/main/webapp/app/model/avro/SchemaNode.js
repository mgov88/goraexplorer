/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Abstract class parent of all avro schema nodes.
 * @abstract
 */
Ext.define('GoraExplorer.model.avro.SchemaNode', {
    extend: 'Ext.data.TreeModel',

    requires: [
        'GoraExplorer.model.avro.visitor.VisitableMixin'
    ],

    mixins: [
        'GoraExplorer.model.avro.visitor.VisitableMixin'
    ],

    fields: [

        /**
         * @field {String} - Type of the node. This is set on the node creation time to the type of the node
         * @readonly
         */
        { name: 'type', type: 'string' },

        /**
         * Name of the Entity/Field. A field name when it belongs to a Record
         */
        { name: 'name', type: 'string' },

        /**
         * Documentation of the field
         */
        { name: 'doc', type: 'string' }

    ]

}) ;