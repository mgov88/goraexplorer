/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Ext.define('GoraExplorer.model.avro.ArrayNode', {
    extend: 'GoraExplorer.model.avro.FieldNode',

    fields: [

        /**
         * @override
         */
        { name: 'type', type: 'string', defaultValue: 'array' },

        /**
         * Type of the items the array will hold: 'boolean', 'int', 'long', 'float', 'double', 'string', 'bytes' or 'record'
         */
        { name: 'itemsType', type: 'string' },

        /**
         * If the items values can be null
         */
        { name: 'itemsNullable', type: 'boolean' },

        /**
         * When the items are records, the name of the record that exists in Entities Tree Panel
         */
        { name: 'itemsRecordName', type: 'string'}

    ],

    /**
     *
     * @property {GoraExplorer.model.avro.SchemaNode} _itemTypeNode - Cache of the reusable node with the actual type.
     */
    _itemTypeNode: null,

    /**
     * @override
     */
    accept: function(visitor) {
        return visitor.visitArrayNode(this) ;
    },

    /**
     * Returns a <i>reused</i> item type node of this map. Take care of not updating it (or only irrelevant properties).
     * @return {GoraExplorer.model.avro.SchemaNode}
     */
    getItemTypeNode: function() {
        var me = this ;

        if (me._itemTypeNode === null) {
            me._itemTypeNode = me.createItemTypeNode() ;
        }
        return me._itemTypeNode ;
    },


    /**
     * Given this ArrayNode, returns a new node instance of the item type
     * @return {GoraExplorer.model.avro.SchemaNode}
     */
    createItemTypeNode: function() {
        var me = this,
            nodeItemsType = me.get('itemsType'),
            nodeItemsNullable = me.get('itemsNullable'),
            nodeItemsRecordName = me.get('itemsRecordName'),
            tmpItemNode = Ext.create('GoraExplorer.model.avro.' + Ext.String.capitalize(nodeItemsType) + 'Node') ;

        // Sadly a little hack :( maybe schema tree should allow children nodes at array and maps, but that would
        // add other complexities (like a map in a map in a map and alike, now not supported)
        tmpItemNode.set('nullable', nodeItemsNullable) ;
        if (nodeItemsType === 'record') {
            tmpItemNode.set('recordFieldTypeName', nodeItemsRecordName) ;
        }

        return tmpItemNode ;
    }

}) ;