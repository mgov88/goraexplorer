/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Ext.define('GoraExplorer.model.Connection', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.identifier.Negative',
        'Ext.data.validator.Inclusion',
        'Ext.data.validator.Length',
        'Ext.data.validator.Presence',
        'GoraExplorer.model.Table',
        'GoraExplorer.model.User'
    ],

    idProperty: 'id',
    identifier: {
        type: 'negative',
        prefix: '' // Without this, the identifier will be created as string-sequence
    },

    fields: [
        { name: 'id', type: 'int' },
        {
            name: 'user',
            type: 'string',
            reference: {
                parent: 'GoraExplorer.model.User',
                role: 'user',
                inverse: {
                    role: 'connections',
                    storeConfig: {
                        trackRemoved: true // BUGFIX when SYNCing
                    }
                }
            }
        },
        { name: 'name', type: 'string' },
        { name: 'description', type: 'string' },
        { name: 'type', type: 'string' },
        { name: 'goraProperties', type: 'string' },
        { name: 'hadoopConfiguration', type: 'string' }
    ],

    validators: {
        user: { type: 'presence' },
        name: [
            { type: 'presence' },
            { type: 'length', min: 5, max: 50}
        ],
        description: [
            { type: 'length', max: 500}
        ],
        type: [
            { type: 'presence' },
            { type: 'inclusion', list: ['HBASE', 'KUDU', 'IGNITE']}
        ]
    },

    proxy: {
        type: 'rest',
        url: GoraExplorer.applicationHelper.getRestUrl('connections'),
        withCredentials: true
    }

});



