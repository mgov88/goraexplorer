#!/bin/bash

bindir=`dirname "$0"`
bindir=`cd "$bin">/dev/null; pwd`

PO_FILES=$bindir/*.po

echo
echo "Creating translation .js files"
echo "______________________________"
echo
for PO_FILE in $PO_FILES
do
    DEST_FILE=`basename "$PO_FILE" .po`.js
    echo -e "  Processing $PO_FILE => \e[94m$DEST_FILE\e[0m"
    echo -n "var json_locale_data = { \"root\": " > $DEST_FILE
    perl $bindir/po2json $PO_FILE >> $DEST_FILE
    echo "} ;" >> $DEST_FILE
done

echo; echo
