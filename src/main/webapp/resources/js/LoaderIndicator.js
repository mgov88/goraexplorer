/*
 * Copyright 2018 Alfonso Nishikawa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

LoaderIndicator = new class {

    // See boostrap.js#loadElement() , .sencha/app/Boot.js#loadElement() and GoraExplorer.Application#init()

    set _expectedProgressEvents(value) {
        this._loadingProgressElement.max = value ;
    }

    get _expectedProgressEvents() {
        return this._loadingProgressElement.max ;
    }

    init() {
        var me = this,
            loadingElement = document.createElement('div') ;
        loadingElement.setAttribute('class', 'loading') ;
        loadingElement.setAttribute('id', 'loading-indicator') ;
        loadingElement.innerHTML = 'Loading GoraExplorer... <progress id="loading-progress" max="0" value="0"/>' ;
        document.body.insertBefore(loadingElement, document.body.childNodes[0]) ;
        me._loadingElement = document.getElementById('loading-indicator') ;
        me._loadingProgressElement = document.getElementById('loading-progress') ;
        me._internalLoadedCount = 0 ;
        me._expectedProgressEvents = window.localStorage.getItem('LoaderIndicator.expectedProgressEvents') || 1;
    }

    incrementProgress() {
        var me = this ;
        // Check if the expected progress events is unknown
        if (me._expectedProgressEvents === 1) {
            me._expectedProgressEvents = Ext.Microloader.manifest.content.build === 'production' ? 10 : 191 ;
        }

        me._loadingProgressElement.value++ ;
        me._internalLoadedCount++ ;
    }

    finishedLoading() {
        var me = this ;

        console.debug('Number of loaded resources: ' + me._internalLoadedCount) ;

        // Hide the indicator
        if (me._internalLoadedCount > me._expectedProgressEvents) {
            console.warn('The loaded files number is greater than expected: ' + me._internalLoadedCount + ' > ' + me._expectedProgressEvents) ;
        }
        if (me._internalLoadedCount != me._expectedProgressEvents) {
            window.localStorage.setItem('LoaderIndicator.expectedProgressEvents', me._internalLoadedCount);
        }
        me._loadingElement.remove() ;
    }

} ;

// Hang to document ready. https://plainjs.com/javascript/events/running-code-when-the-document-is-ready-15/
document.addEventListener('DOMContentLoaded', function(){
    LoaderIndicator.init() ;
});